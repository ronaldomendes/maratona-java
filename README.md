# Maratona Java - DevDojo

Repositório com o conteúdo desenvolvido no curso **Maratona Java** do canal **DevDojo**, no **YouTube**.
Todo o conteúdo do curso está disponível neste **[link](https://www.youtube.com/playlist?list=PL62G310vn6nHrMr1tFLNOYP_c73m6nAzL)**.

## Conteúdo Apresentado no Curso

01. Basics
02. Variables
03. Operators
04. Arrays
05. Classes
06. Methods
07. Override Methods
08. Initialization
09. Static Modifier
10. Association
11. Inheritance
12. Override
13. Final Modifier
14. Enumerators
15. Abstract Classes
16. Interfaces
17. Polymorphism
18. Exceptions
19. Assertions
20. Wrappers
21. Strings
22. Date Format
23. Regex
24. Resource Bundle
25. Input / Output
26. New IO
27. Serialization
28. Collections
29. Generics
30. Inner Classes
31. JDBC
32. Threads
33. Concurrent
34. Design Patterns
35. Behavior Parametrization
36. Lambdas
37. Default Methods
38. Optional
39. Streams
40. Parallel Streams
41. Completable Future
42. Datetime