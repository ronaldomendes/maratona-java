package cursojava.cap01_basics;

public class RelatorioPendencia {
    public static void main(String[] args) {
        String nome = "Godofredo";
        String endereco = "Rua da Comilança, 55";
        String telefone = "11 987654321";

        String res = String.format("O %s domiciliado no endereço %s e telefone %s não possui nenhum tipo de pendência.",
                nome, endereco, telefone);

        System.out.println(res);
    }
}
