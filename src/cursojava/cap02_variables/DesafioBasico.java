package cursojava.cap02_variables;

public class DesafioBasico {
    public static void main(String[] args) {
        String nome = "Creuza";
        double salario = 2500;
        char sexo = 'F';
        int idade = 28;
        String estadoCivil = "Solteira";

        String res = String.format("O trabalhador(a) %s do sexo %s, idade %d, estado civil %s " +
                        "e salario %.2f encontra-se empregado neste estabelecimento",
                nome, sexo, idade, estadoCivil, salario);
        System.out.println(res);
    }
}
