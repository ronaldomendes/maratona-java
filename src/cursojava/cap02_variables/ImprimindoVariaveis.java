package cursojava.cap02_variables;

public class ImprimindoVariaveis {
    public static void main(String[] args) {
        int idade = 10;
        double salarioDouble = 3000;
        float salarioFloat = 3000;
        byte idadeByte = 12;
        short idadeShort = 12;
        boolean verdadeiro = true;
        boolean falso = false;
        long numeroGrande = 1000L;
        char caractere = 95;
        String nome = "Godofredo";

        System.out.println("Dados impressos");
        System.out.println(idade);
        System.out.println(salarioDouble);
        System.out.println(salarioFloat);
        System.out.println(idadeByte);
        System.out.println(idadeShort);
        System.out.println(verdadeiro);
        System.out.println(falso);
        System.out.println(numeroGrande);
        System.out.println(caractere);
        System.out.println(nome);
    }
}
