package cursojava.cap03_operators;

public class DesafioLoops {
    public static void main(String[] args) {
        int num = 100000;
        int iteration = 2;
        System.out.println("Inicio...");
        for (int i = 0; i <= num; i += iteration) {
            System.out.println(i);
        }
        System.out.println("Fim...");
    }
}
