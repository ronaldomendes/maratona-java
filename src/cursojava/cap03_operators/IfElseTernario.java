package cursojava.cap03_operators;

public class IfElseTernario {
    public static void main(String[] args) {
        int idade = 18;
        String status = "Categoria ";
        status += idade < 15 ? "Infantil" : idade >= 15 && idade < 18 ? "Juvenil" : "Adulto";
        System.out.println(status);
    }
}
