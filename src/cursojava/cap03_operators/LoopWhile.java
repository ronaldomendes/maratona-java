package cursojava.cap03_operators;

public class LoopWhile {
    public static void main(String[] args) {
        int contador = 0;
        while (contador < 10) {
//            System.out.println(contador);
            contador++;
        }

        int cont2 = 0;
        do {
//            System.out.println(cont2);
            cont2++;
        } while (cont2 < 5);

        for (int i = 0; i < 10; i += 2) {
            System.out.println(i);
            if (i == 4) {
                break;
            }
        }
    }
}
