package cursojava.cap03_operators;

public class OperadoresAtribuicao {
    public static void main(String[] args) {
        double salario = 1000;
        salario += 1000;
        System.out.println(salario);
        salario -= 100;
        System.out.println(salario);
        salario *= 1.10;
        System.out.println(salario);
        salario /= 2;
        System.out.println(salario);
        salario %= 2;
        System.out.println(salario);

    }
}
