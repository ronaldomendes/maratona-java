package cursojava.cap03_operators;

public class OperadoresComparacao {
    public static void main(String[] args) {
        boolean dezMaiorQueVinte = 10 > 20;
        boolean dezMaiorOuIgualAVinte = 10 >= 20;
        boolean dezMenorQueVinte = 10 < 20;
        boolean dezMenorOuIgualAVinte = 10 <= 20;
        boolean dezIgualAVinte = 10 == 20;

        System.out.println(dezMaiorQueVinte);
        System.out.println(dezMaiorOuIgualAVinte);
        System.out.println(dezMenorQueVinte);
        System.out.println(dezMenorOuIgualAVinte);
        System.out.println(dezIgualAVinte);
    }
}
