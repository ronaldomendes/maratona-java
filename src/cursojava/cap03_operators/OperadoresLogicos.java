package cursojava.cap03_operators;

public class OperadoresLogicos {
    public static void main(String[] args) {
        int idade = 18;
        float salario = 1000f;

        boolean resp = idade >= 18 || salario >= 3000;

        System.out.println(resp);
    }
}
