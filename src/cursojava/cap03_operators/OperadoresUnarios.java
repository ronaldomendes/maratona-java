package cursojava.cap03_operators;

public class OperadoresUnarios {
    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 20;

        int adicao = num1 + num2;
        int subtracao = num1 - num2;
        int multiplicacao = num1 * num2;
        double divisao =  (double) num1 / (double) num2;
        int resto = 20 % 2;

        System.out.println(adicao);
        System.out.println(subtracao);
        System.out.println(multiplicacao);
        System.out.println(divisao);
        System.out.println(resto);
    }
}
