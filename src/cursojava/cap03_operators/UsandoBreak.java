package cursojava.cap03_operators;

public class UsandoBreak {
    public static void main(String[] args) {
        /*
        dado um valor de um carro descubra em quantas vezes ele pode ser parcelado
        porém as parcelas não podem ser menores do que 1000
        */
        double valorTotal = 30000;
        for (int parcela = 1; parcela <= valorTotal; parcela++) {
            double valorParcela = valorTotal / parcela;
            if (valorParcela < 1000) {
                break;
            }
            System.out.println(String.format("Parcela %d R$ %.2f", parcela, valorParcela));
        }

    }

}
