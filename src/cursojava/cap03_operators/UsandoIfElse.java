package cursojava.cap03_operators;

public class UsandoIfElse {
    public static void main(String[] args) {
        int idade = 18;

        if (idade < 17) {
            System.out.println("Você é adolescente");
        } else {
            System.out.println("Você é adulto");
        }
    }
}
