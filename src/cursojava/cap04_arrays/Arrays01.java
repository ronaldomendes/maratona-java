package cursojava.cap04_arrays;

public class Arrays01 {
    public static void main(String[] args) {
        int[] idades = new int[3];
        idades[0] = 20;
        idades[1] = 15;
        idades[2] = 30;

        for (int idade : idades) {
            System.out.println(idade);
        }
    }
}
