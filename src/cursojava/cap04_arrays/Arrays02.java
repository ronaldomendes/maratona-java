package cursojava.cap04_arrays;

public class Arrays02 {
    public static void main(String[] args) {
        // os tipos byte, short, int, long, float e double iniciam com 0
        // o tipo char inicia com "\u0000" ou ""
        // o tipo boolean inicia com false
        // em um tipo reference ex: String, inicia com null

        String[] dados = new String[3];
        dados[0] = "Creuza";
        dados[1] = "Godofredo";
        dados[2] = "Tonho";

        for (String dado : dados) {
            System.out.println(dado);
        }
    }
}
