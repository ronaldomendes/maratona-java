package cursojava.cap04_arrays;

public class Arrays03 {
    public static void main(String[] args) {
//        formas de criar um array
        int[] nums01 = new int[5];
        int[] nums02 = {1, 2, 3, 4, 5};
        int[] nums03 = new int[]{1, 2, 3, 4, 5};

        for (int num : nums02) {
            System.out.println(num);
        }
    }
}
