package cursojava.cap04_arrays;

public class Arrays04 {
    public static void main(String[] args) {
        String[] nomes = {"Huguinho", "Luizinho", "Zezinho"};

        for (String nome : nomes) {
            System.out.println(nome);
        }
    }
}
