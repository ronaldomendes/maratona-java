package cursojava.cap04_arrays;

public class ArraysBidimensionais {
    public static void main(String[] args) {
        int[][] dias = new int[2][2];
        dias[0][0] = 30;
        dias[0][1] = 31;
        dias[1][0] = 29;
        dias[1][1] = 28;

        for (int[] x : dias) {
            for (int y : x) {
                System.out.println(y);
            }
        }
    }
}
