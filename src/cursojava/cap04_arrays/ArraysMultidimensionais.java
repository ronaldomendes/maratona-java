package cursojava.cap04_arrays;

public class ArraysMultidimensionais {
    public static void main(String[] args) {
        int[][] nums01 = new int[3][];
        nums01[0] = new int[2];
        nums01[1] = new int[]{1, 2, 3};
        nums01[2] = new int[4];

        for (int[] arr : nums01) {
            for (int num : arr) {
                System.out.println(num);
            }
        }

        int[][] nums02 = {{0, 0}, {1, 2, 3}, {0, 0, 0, 0}};

    }
}
