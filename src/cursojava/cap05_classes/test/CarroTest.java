package cursojava.cap05_classes.test;

import cursojava.cap05_classes.entity.Carro;

public class CarroTest {
    public static void main(String[] args) {
        Carro carro = new Carro();

        carro.placa = "ABC1234";
        carro.modelo = "Fusca 1600";
        carro.velocidadeMaxima = 140.00;

        System.out.println(carro.placa);
        System.out.println(carro.modelo);
        System.out.println(carro.velocidadeMaxima);
    }
}
