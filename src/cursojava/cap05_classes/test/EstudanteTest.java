package cursojava.cap05_classes.test;

import cursojava.cap05_classes.entity.Estudante;

public class EstudanteTest {
    public static void main(String[] args) {
        Estudante estudante = new Estudante();

        estudante.nome = "Zezinho";
        estudante.idade = 25;
        estudante.matricula = "ABC123";

        System.out.println(estudante.nome);
        System.out.println(estudante.idade);
        System.out.println(estudante.matricula);
    }
}
