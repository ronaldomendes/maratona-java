package cursojava.cap05_classes.test;

import cursojava.cap05_classes.entity.Professor;

public class ProfessorTest {
    public static void main(String[] args) {
        Professor professor = new Professor();

        professor.nome = "Godofredo";
        professor.matricula = "ABC2020";
        professor.rg = "123406570";
        professor.cpf = "12345678920";

        System.out.println(professor.nome);
        System.out.println(professor.matricula);
        System.out.println(professor.rg);
        System.out.println(professor.cpf);
    }
}
