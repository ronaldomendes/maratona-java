package cursojava.cap06_methods.entity;

public class Calculadora {
    public static void somaDoisNumeros() {
        System.out.println(5 + 5);
    }

    public static void subtraiDoisNumeros() {
        System.out.println(7 - 2);
    }

    public static void multiplicaDoisNumeros(int a, int b) {
        System.out.println(a * b);
    }

    public static double divideDoisNumeros(double a, double b) {
        double resultado = a / b;
        return b != 0 ? resultado : 0;
    }

    public static void imprimeDoisNumerosDivididos(double a, double b) {
        if (b != 0) {
            System.out.println(a / b);
            return;
        }
        System.out.println("?????");
    }

    public static void alteraDoisNumeros(int num1, int num2) {
        num1 = 30;
        num2 = 40;

        System.out.println("Alterando os números...");
        System.out.println("Num 01: " + num1);
        System.out.println("Num 02: " + num2);
    }

    public static void somaArray(int[] nums) {
        int soma = 0;
        for (int n : nums) {
            soma += n;
        }
        System.out.println(soma);
    }

    public static void somaVarArgs(int... nums) {
        int soma = 0;
        for (int n : nums) {
            soma += n;
        }
        System.out.println(soma);
    }
}
