package cursojava.cap06_methods.entity;

public class Estudante {
    private String nome;
    private int idade;
    private double[] notas;

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public double[] getNotas() {
        return notas;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setIdade(int idade) {
        if (idade < 0) {
            System.out.println("Idade incorreta!");
            return;
        }
        this.idade = idade;
    }

    public void setNotas(double[] notas) {
        this.notas = notas;
    }

    public void imprimeDados() {
        System.out.println("\n");
        System.out.println("Nome: " + this.nome);
        System.out.println("Idade: " + this.idade);

        if (this.notas != null) {
            System.out.print("Notas: ");
            for (double nota : notas) {
                System.out.print(nota + " ");
            }
        }

        if (this.notas != null) {
            double media = 0.0;
            for (double nota : this.notas) {
                media += nota;
            }
            media = media / this.notas.length;
            if (media > 6) {
                System.out.println("\nAprovado com média: " + media);
            } else {
                System.out.println("\nReprovado com média: " + media);
            }
        }
    }

    public void mostrarMedia() {
        if (this.notas == null) {
            System.out.println("Esse aluno não possui notas");
            return;
        }

        double media = 0.0;
        for (double nota : this.notas) {
            media += nota;
        }
        media = media / this.notas.length;
        if (media > 6) {
            System.out.println("\nAprovado com média: " + media);
        } else {
            System.out.println("\nReprovado com média: " + media);
        }
    }
}
