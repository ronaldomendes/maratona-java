package cursojava.cap06_methods.test;

import cursojava.cap06_methods.entity.Calculadora;

public class CalculadoraTest {
    public static void main(String[] args) {
//        Calculadora calc = new Calculadora();
        System.out.println("Soma...");
        Calculadora.somaDoisNumeros();

        System.out.println("Subtração...");
        Calculadora.subtraiDoisNumeros();

        System.out.println("Multiplicação...");
        Calculadora.multiplicaDoisNumeros(6, 4);

        System.out.println("Divisão 1...");
        System.out.println(Calculadora.divideDoisNumeros(15, 5));

        System.out.println("Divisão 2...");
        Calculadora.imprimeDoisNumerosDivididos(5, 0);

        System.out.println("Calculadora Array");
        int[] nums = {2, 3, 5, 6, 7, 9, 12};
        Calculadora.somaArray(nums);

        System.out.println("Calculadora Com Args");
        Calculadora.somaVarArgs(2, 6, 4, 8, 84, 23, 5, 8, 3);
    }
}
