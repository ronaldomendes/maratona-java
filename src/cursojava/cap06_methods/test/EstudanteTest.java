package cursojava.cap06_methods.test;

import cursojava.cap06_methods.entity.Estudante;

public class EstudanteTest {
    public static void main(String[] args) {
        Estudante est = new Estudante();
        est.setNome("Creuza");
        est.setIdade(22);
        est.setNotas(new double[]{8, 8, 8});

        est.imprimeDados();
        est.mostrarMedia();
    }
}
