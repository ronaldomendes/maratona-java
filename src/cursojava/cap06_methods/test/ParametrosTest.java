package cursojava.cap06_methods.test;

import cursojava.cap06_methods.entity.Calculadora;

public class ParametrosTest {
    public static void main(String[] args) {
        Calculadora calc = new Calculadora();

        int num1 = 10;
        int num2 = 5;
        calc.alteraDoisNumeros(num1, num2);

        System.out.println("Depois do método...");
        System.out.println("Num 01: " + num1);
        System.out.println("Num 02: " + num2);
    }
}
