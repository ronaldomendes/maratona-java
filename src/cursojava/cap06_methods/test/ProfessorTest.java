package cursojava.cap06_methods.test;

import cursojava.cap06_methods.entity.Professor;

public class ProfessorTest {
    public static void main(String[] args) {
        Professor prof01 = new Professor();
        prof01.nome = "Obi Wan Kenobi";
        prof01.matricula = "T01D02R";
        prof01.rg = "12.321.654-98";
        prof01.cpf = "321.654.789-60";

        Professor prof02 = new Professor();
        prof02.nome = "General Grievous";
        prof02.matricula = "T04D17R";
        prof02.rg = "12.321.654-98";
        prof02.cpf = "321.654.789-60";

        prof01.imprime();
        prof02.imprime();

    }
}
