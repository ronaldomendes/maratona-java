package cursojava.cap07_override_methods.classes;

public class Funcionario {

    private String nome;
    private String cpf;
    private String rg;
    private double salario;

    public Funcionario() {
    }

    public Funcionario(String nome, String cpf, String rg, double salario) {
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.salario = salario;
    }

    public void init(String nome, String cpf, double salario) {
        this.nome = nome;
        this.cpf = cpf;
        this.salario = salario;
    }

    public void init(String nome, String cpf, String rg, double salario) {
        init(nome, cpf, salario);
        this.rg = rg;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public void imprime() {
        System.out.println(this.getNome());
        System.out.println(this.getCpf());
        System.out.println(this.getRg());
        System.out.println(this.getSalario());
    }
}
