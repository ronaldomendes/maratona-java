package cursojava.cap07_override_methods.test;

import cursojava.cap07_override_methods.classes.Estudante;

public class EstudanteTest {
    public static void main(String[] args) {
        Estudante estudante = new Estudante("ABC23072020", "Zequinha", new double[]{9, 8.5, 7.5, 9}, "11-09-2001");
        estudante.imprime();
    }
}
