package cursojava.cap07_override_methods.test;

import cursojava.cap07_override_methods.classes.Funcionario;

public class FuncionarioTest {
    public static void main(String[] args) {
        Funcionario func = new Funcionario();

        func.setNome("Zeca Urubu");
        func.setCpf("321.654.788-50");
        func.setRg("50.546.123-23");
        func.setSalario(4500.00);
        func.imprime();

        Funcionario func02 = new Funcionario("Tião Carreiro", "123.456.789-50" , "12.123.456-50", 3000.00);
//        func02.init("Tião Carreiro", "123.456.789-50" , "12.123.456-50", 3000.00);
        func02.imprime();
    }
}
