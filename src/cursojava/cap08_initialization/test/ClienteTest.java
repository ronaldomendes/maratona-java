package cursojava.cap08_initialization.test;

import cursojava.cap08_initialization.classes.Cliente;

public class ClienteTest {
    public static void main(String[] args) {
        Cliente cliente = new Cliente();
        System.out.println("Exibindo quantidade de parcelas possíveis");

        for (int p: cliente.getParcelas()) {
            System.out.print(p + " ");
        }

    }
}
