package cursojava.cap09_static_modifier.classes;

public class Cliente {
    /*
    1- O Bloco de inicialização é executado quando a JVM carregar a classe
        (o bloco estático é executado apenas uma vez enquanto um bloco não estático é executado a
        cada nova instância da classe)
    2- Alocado espaço na memória para o objeto que será criado
    3- Cada atributo de classe é criado e inicializado com seus valores default ou valores explícitos
    4- Bloco de inicialização é executado
    5- O construtor é executado
     */

    private static int[] parcelas;

    {
        System.out.println("Não sou um bloco estático");
    }

    static {
        parcelas = new int[100];
        System.out.println("Dentro do bloco de inicialização");
        for (int i = 1; i <= 100; i++) {
            parcelas[i - 1] = i;
        }
    }

    static {
        System.out.println("Sou o segundo bloco estático");
    }

    public Cliente() {
    }

    public static int[] getParcelas() {
        return parcelas;
    }

//    public void setParcelas(int[] parcelas) {
//        Cliente.parcelas = parcelas;
//    }
}
