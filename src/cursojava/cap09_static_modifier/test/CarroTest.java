package cursojava.cap09_static_modifier.test;

import cursojava.cap09_static_modifier.classes.Carro;

public class CarroTest {
    public static void main(String[] args) {
        Carro carro01 = new Carro("Fusca 1500", 250);
        Carro carro02 = new Carro("Gol G3", 250);
        Carro carro03 = new Carro("Fiat 147", 320);

        carro01.imprime();
        carro02.imprime();
        carro03.imprime();


        Carro.setVelocidadeLimite(150);
        System.out.println("Alterando os dados...");
        carro01.imprime();
        carro02.imprime();
        carro03.imprime();
    }
}
