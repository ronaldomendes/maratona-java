package cursojava.cap09_static_modifier.test;

import cursojava.cap09_static_modifier.classes.Cliente;

public class ClienteTest {
    public static void main(String[] args) {
        Cliente cliente = new Cliente();
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();
        System.out.println("Exibindo quantidade de parcelas possíveis");

//        for (int p : cliente.getParcelas()) {
//            System.out.print(p + " ");
//        }
        System.out.println("Parcelas " + Cliente.getParcelas().length);
    }
}
