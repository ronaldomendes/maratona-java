package cursojava.cap10_association.desafio;

public class Professor {
    private String nome;
    private String especialidade;
    private Seminario[] seminarios;

    public Professor() {
    }

    public Professor(String nome, String especialidade) {
        this.nome = nome;
        this.especialidade = especialidade;
    }

    public void print() {
        System.out.println("\nProfessor: " + this.nome);
        System.out.println("Especialidade: " + this.especialidade);
        if (this.seminarios != null && this.seminarios.length != 0) {
            System.out.println("Seminários: ");
            for (Seminario seminario : seminarios) {
                System.out.println("\t" + seminario.getTitulo());
            }
        } else {
            System.out.println("Nenhum seminário cadastrado");
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public Seminario[] getSeminarios() {
        return seminarios;
    }

    public void setSeminarios(Seminario[] seminarios) {
        this.seminarios = seminarios;
    }
}
