package cursojava.cap10_association.test;

import cursojava.cap10_association.desafio.Aluno;
import cursojava.cap10_association.desafio.Local;
import cursojava.cap10_association.desafio.Professor;
import cursojava.cap10_association.desafio.Seminario;

public class AssociacaoTest {
    public static void main(String[] args) {
        Aluno aluno01 = new Aluno("Anakin Skywalker", 19);
        Aluno aluno02 = new Aluno("Jar Jar Binks", 22);
        Seminario seminario = new Seminario("Técnicas de Programação em Java 11");
        Professor professor = new Professor("Obi Wan Kenobi", "Programador Jedi");
        Local local = new Local("Rua MMXX, 2020", "Tatooine");

        aluno01.setSeminario(seminario);
        aluno02.setSeminario(seminario);

//        seminario.setProfessor(professor);
//        seminario.setLocal(local);
//        seminario.setAlunos(new Aluno[]{aluno01, aluno02});

        professor.setSeminarios(new Seminario[] {seminario});

        aluno01.print();
        aluno02.print();

        professor.print();
        local.print();
        seminario.print();
    }
}
