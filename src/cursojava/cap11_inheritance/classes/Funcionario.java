package cursojava.cap11_inheritance.classes;

public class Funcionario extends Pessoa {
    private double salario;

    public Funcionario() {
    }

    public Funcionario(String nome, double salario) {
        super(nome);
        System.out.println("Dentro do construtor (Funcionário)");
        this.salario = salario;
    }

    public Funcionario(String nome, String cpf, double salario) {
        super(nome, cpf);
        System.out.println("Dentro do construtor (Funcionário)");
        this.salario = salario;
    }

    public Funcionario(String nome, String cpf, Endereco endereco, double salario) {
        super(nome, cpf, endereco);
        System.out.println("Dentro do construtor (Funcionário)");
        this.salario = salario;
    }

    {
        System.out.println("Bloco de inicialização (Funcionário) 1");
    }

    static {
        System.out.println("Bloco de inicialização estático (Funcionário)");
    }

    {
        System.out.println("Bloco de inicialização (Funcionário) 2");
    }

    public void imprime() {
        super.imprime();
        System.out.println("Salário: " + this.salario);
        imprimeReciboPagamento();
    }

    public void imprimeReciboPagamento() {
        System.out.println(String.format("Eu, %s recebi o valor de R$ %.2f", super.nome, this.salario));
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
