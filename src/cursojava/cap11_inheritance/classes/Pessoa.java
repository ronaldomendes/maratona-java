package cursojava.cap11_inheritance.classes;

//Toda classe mãe extende a classe Object
public class Pessoa {
    protected String nome;
    protected String cpf;
    protected Endereco endereco;

    public Pessoa() {
    }

    public Pessoa(String nome) {
        System.out.println("Dentro do construtor (Pessoa)");
        this.nome = nome;
    }

    static {
        System.out.println("Bloco de inicialização estático (Pessoa)");
    }

    {
        System.out.println("Bloco de inicialização 1 (Pessoa)");
    }

    {
        System.out.println("Bloco de inicialização 2 (Pessoa)");
    }

    public Pessoa(String nome, String cpf) {
        System.out.println("Dentro do construtor (Pessoa)");
        this.nome = nome;
        this.cpf = cpf;
    }

    public Pessoa(String nome, String cpf, Endereco endereco) {
        System.out.println("Dentro do construtor (Pessoa)");
        this.nome = nome;
        this.cpf = cpf;
        this.endereco = endereco;
    }

    public void imprime() {
        System.out.println("\nNome: " + this.nome);
        System.out.println("CPF: " + this.cpf);
        System.out.println("Endereço: " + this.endereco.getRua() + " - Bairro: " + this.endereco.getBairro());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
