package cursojava.cap11_inheritance.test;

import cursojava.cap11_inheritance.classes.Endereco;
import cursojava.cap11_inheritance.classes.Funcionario;
import cursojava.cap11_inheritance.classes.Pessoa;

public class HerancaTest {
    public static void main(String[] args) {
        /*
        1- Espaço em memória é alocado para o objeto sendo construído
        2- Cada um dos atributos do objeto é criado e inicializado com os valores default
        3- O construtor da superclasse é chamado
        4- A inicialização dos atributos via declaração e o código do bloco de inicialização da superclasse são
        executados na ordem em que aparecem
        5- O código do construtor da superclasse é chamado
        6- Passo 4 para a subclasse é executado
        7- O código do construtor da classe é executado
         */

        Endereco endereco = new Endereco("Rua Dois, 3", "Naboo");
//        Pessoa pessoa = new Pessoa("Pica Pau Biruta", "321.456.879-98", endereco);
//        pessoa.imprime();

        Funcionario funcionario = new Funcionario("Godofredo", "321.123.654-50", endereco, 5230.00);
//        funcionario.imprime();
    }
}
