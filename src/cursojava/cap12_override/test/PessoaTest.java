package cursojava.cap12_override.test;

import cursojava.cap12_override.classes.Pessoa;

public class PessoaTest {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Godofredo");
        pessoa.setIdade(44);
        System.out.println(pessoa);
    }
}
