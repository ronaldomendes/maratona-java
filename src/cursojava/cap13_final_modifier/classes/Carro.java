package cursojava.cap13_final_modifier.classes;

public class Carro {
    public static final double VELOCIDADE_FINAL = 200;
    public final Comprador COMPRADOR = new Comprador();
    private String nome;
    private String marca;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Carro{");
        sb.append("nome='").append(nome).append('\'');
        sb.append(", marca='").append(marca).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public final void imprime() {
        System.out.println("Imprimindo um novo carro...");
    }
}
