package cursojava.cap13_final_modifier.test;

import cursojava.cap13_final_modifier.classes.Carro;
import cursojava.cap13_final_modifier.classes.Subaru;

public class CarroTest {
    public static void main(String[] args) {
        Carro carro = new Carro();
        System.out.println(carro.COMPRADOR);
        carro.COMPRADOR.setNome("Clovis");
        System.out.println(carro.COMPRADOR.getNome());
        Subaru subaru = new Subaru();
        subaru.imprime();
        subaru.imprime("Teste...");
    }
}
