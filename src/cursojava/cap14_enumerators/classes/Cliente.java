package cursojava.cap14_enumerators.classes;

public class Cliente {
    public enum TipoPagamento {
        A_VISTA, A_PRAZO
    }

    private String nome;
    private TipoCliente tipoCliente;
    private TipoPagamento pagamento;

    public Cliente() {
    }

    public Cliente(String nome, TipoCliente tipoCliente, TipoPagamento pagamento) {
        this.nome = nome;
        this.tipoCliente = tipoCliente;
        this.pagamento = pagamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoCliente getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoCliente tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public TipoPagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(TipoPagamento pagamento) {
        this.pagamento = pagamento;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Cliente{");
        sb.append("nome='").append(nome).append('\'');
        sb.append(", tipo=").append(tipoCliente.getNome());
        sb.append(", pagamento=").append(pagamento);
        sb.append(", número=").append(tipoCliente.getTipo());
        sb.append('}');
        return sb.toString();
    }
}
