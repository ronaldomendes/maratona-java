package cursojava.cap14_enumerators.test;

import cursojava.cap14_enumerators.classes.Cliente;
import cursojava.cap14_enumerators.classes.TipoCliente;

public class ClienteTest {
    public static void main(String[] args) {
        Cliente cliente = new Cliente("Creuza", TipoCliente.PESSOA_JURIDICA, Cliente.TipoPagamento.A_PRAZO);
        System.out.println(cliente);
        System.out.println(TipoCliente.PESSOA_FISICA.getId());
        System.out.println(TipoCliente.PESSOA_JURIDICA.getId());
    }
}
