package cursojava.cap15_abstract_classes.classes;

public abstract class Funcionario extends Pessoa {
    protected String clt;
    protected double salario;

    public Funcionario() {
    }

    public Funcionario(String nome, String clt, double salario) {
        this.nome = nome;
        this.clt = clt;
        this.salario = salario;
    }

    public abstract void calculaSalario();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClt() {
        return clt;
    }

    public void setClt(String clt) {
        this.clt = clt;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Funcionario{");
        sb.append("nome='").append(nome).append('\'');
        sb.append(", clt='").append(clt).append('\'');
        sb.append(", salario=").append(salario);
        sb.append('}');
        return sb.toString();
    }

    public void imprime() {
        /* se eu implementar um método de uma classe abstrata em outra classe abstrata,
        não existirá mais a obrigatoriedade de sobreescrita nas classes filhas
        */
        System.out.println("Imprimindo na classe Funcionário");
    }
}
