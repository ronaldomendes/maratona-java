package cursojava.cap15_abstract_classes.classes;

public abstract class Pessoa {
    protected String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public abstract void imprime();
}
