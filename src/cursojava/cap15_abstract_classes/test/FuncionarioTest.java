package cursojava.cap15_abstract_classes.test;

import cursojava.cap15_abstract_classes.classes.Funcionario;
import cursojava.cap15_abstract_classes.classes.Gerente;
import cursojava.cap15_abstract_classes.classes.Vendedor;

public class FuncionarioTest {
    public static void main(String[] args) {
//        Funcionario funcionario = new Funcionario("Pato Donald", "2020-01", 3525.00);
        Gerente gerente = new Gerente("Margarida", "2020-01", 2000.00);
        Vendedor vendedor = new Vendedor("Pato Donald", "2020-01", 1500, 5000);
        vendedor.calculaSalario();
        System.out.println(vendedor);

        gerente.calculaSalario();
        System.out.println(gerente);
    }
}
