package cursojava.cap16_interfaces.classes;

public class Carro implements Tributavel, Transportavel {
    @Override
    public void calculaFrete() {

    }

    @Override
    public void calculaImposto() {

    }
}
