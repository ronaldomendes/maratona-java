package cursojava.cap16_interfaces.classes;

public interface Tributavel {
    //    cada variável criada em uma interface é "public static final"
    public static final double IMPOSTO = 0.2;

    //    cada metodo criado em uma interface é "public abstract"
    public abstract void calculaImposto();
}
