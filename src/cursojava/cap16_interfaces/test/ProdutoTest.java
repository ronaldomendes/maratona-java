package cursojava.cap16_interfaces.test;

import cursojava.cap16_interfaces.classes.Produto;

public class ProdutoTest {
    public static void main(String[] args) {
        Produto produto = new Produto("Notebook Posilixo", 4, 3000);
        produto.calculaImposto();
        produto.calculaFrete();
        System.out.println(produto);
    }
}
