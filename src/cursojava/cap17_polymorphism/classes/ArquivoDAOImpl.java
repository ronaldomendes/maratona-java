package cursojava.cap17_polymorphism.classes;

public class ArquivoDAOImpl implements GenericDataAccessObject {
    @Override
    public void save() {
        System.out.println("Salvando dados nos arquivos");
    }
}
