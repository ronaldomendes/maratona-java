package cursojava.cap17_polymorphism.classes;

public class DatabaseDAOImpl implements GenericDataAccessObject {
    @Override
    public void save() {
        System.out.println("Salvando dados no BD");
    }
}
