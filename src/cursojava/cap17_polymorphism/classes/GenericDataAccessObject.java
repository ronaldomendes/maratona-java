package cursojava.cap17_polymorphism.classes;

public interface GenericDataAccessObject {
    void save();
}
