package cursojava.cap17_polymorphism.classes;

public class Gerente extends Funcionario {
    private double plr;

    public Gerente(String nome, double salario, double plr) {
        super(nome, salario);
        this.plr = plr;
    }

    public double getPlr() {
        return plr;
    }

    public void setPlr(double plr) {
        this.plr = plr;
    }

    @Override
    public void calculaPagamento() {
        this.salario += this.plr;
    }
}
