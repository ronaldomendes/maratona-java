package cursojava.cap17_polymorphism.classes;

public class RelatorioPagamento {
//    public void relatorioPagamentoGerente(Gerente gerente) {
//        System.out.println("\nGerando relatório de pagamento para a gerência");
//        gerente.calculaPagamento();
//        System.out.println("Nome: " + gerente.getNome());
//        System.out.println("Salário: R$ " + gerente.getSalario());
//    }
//
//    public void relatorioPagamentoVendedor(Vendedor vendedor) {
//        System.out.println("\nGerando relatório de pagamento para os vendedores");
//        vendedor.calculaPagamento();
//        System.out.println("Nome: " + vendedor.getNome());
//        System.out.println("Salário: R$ " + vendedor.getSalario());
//    }

    public void relatorioPagamentoGenerico(Funcionario funcionario) {
        System.out.println("\nGerando relatório de pagamento");
        funcionario.calculaPagamento();
        System.out.println("Nome: " + funcionario.getNome());
        System.out.println("Salário: R$ " + funcionario.getSalario());

        if (funcionario instanceof Gerente) {
            Gerente gerente = (Gerente) funcionario;
            System.out.println("Participação nos lucros: R$ " + gerente.getPlr());
        }

        if (funcionario instanceof Vendedor) {
            System.out.println("Total vendas: R$ " + ((Vendedor) funcionario).getTotalVendas());
        }
    }
}
