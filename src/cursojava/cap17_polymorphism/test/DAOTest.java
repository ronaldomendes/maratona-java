package cursojava.cap17_polymorphism.test;

import cursojava.cap17_polymorphism.classes.ArquivoDAOImpl;
import cursojava.cap17_polymorphism.classes.DatabaseDAOImpl;
import cursojava.cap17_polymorphism.classes.GenericDataAccessObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DAOTest {
    public static void main(String[] args) {
        ArquivoDAOImpl arquivoDAO = new ArquivoDAOImpl();
        arquivoDAO.save();

        GenericDataAccessObject arquivo01 = new ArquivoDAOImpl();
        GenericDataAccessObject arquivo02 = new DatabaseDAOImpl();
        arquivo01.save();
        arquivo02.save();

//        List<String> lista = new ArrayList<>();
        List<String> lista = new LinkedList<>();
        lista.add("nome 01");
        lista.add("nome 02");
        lista.add("nome 03");
        lista.add("nome 04");
        lista.add("nome 05");

        for (String l : lista) {
            System.out.println(l);
        }
    }
}
