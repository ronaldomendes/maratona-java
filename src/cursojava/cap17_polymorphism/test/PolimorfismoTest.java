package cursojava.cap17_polymorphism.test;

import cursojava.cap17_polymorphism.classes.Funcionario;
import cursojava.cap17_polymorphism.classes.Gerente;
import cursojava.cap17_polymorphism.classes.RelatorioPagamento;
import cursojava.cap17_polymorphism.classes.Vendedor;

public class PolimorfismoTest {
    public static void main(String[] args) {
        Gerente gerente = new Gerente("Tio Patinhas", 5000, 2000);
        Vendedor vendedor = new Vendedor("Pato Donald", 2000, 20000);
        RelatorioPagamento relatorio = new RelatorioPagamento();
//        relatorio.relatorioPagamentoGerente(gerente);
//        relatorio.relatorioPagamentoVendedor(vendedor);

        relatorio.relatorioPagamentoGenerico(gerente);
        relatorio.relatorioPagamentoGenerico(vendedor);

//        Funcionario funcionario = gerente;
//        System.out.println("\n#####################");
//        System.out.println(funcionario.getSalario());
    }
}
