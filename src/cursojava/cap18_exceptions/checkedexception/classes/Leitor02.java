package cursojava.cap18_exceptions.checkedexception.classes;

public class Leitor02 implements AutoCloseable {
    @Override
    public void close() throws Exception {
        System.out.println("Fechando leitor 02");
    }
}
