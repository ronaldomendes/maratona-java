package cursojava.cap18_exceptions.checkedexception.test;

import java.io.File;
import java.io.IOException;

public class CheckedExceptionTest {
    public static void main(String[] args) {
        try {
            criarArquivo();
            abrirArquivo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void criarArquivo() throws IOException {
        File file = new File("teste.txt");
        try {
            System.out.println("Arquivo criado? " + file.createNewFile());
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static void abrirArquivo() {
        try {
            System.out.println("Abrindo um arquivo...");
            System.out.println("Lendo um arquivo...");
            throw new Exception();
//            System.out.println("Editando um arquivo...");
        } catch (Exception e) {
            System.out.println("Pegou uma exception...");
            e.printStackTrace();
        } finally {
            System.out.println("Fechando um arquivo...");
        }
    }
}
