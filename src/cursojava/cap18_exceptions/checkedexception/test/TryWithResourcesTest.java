package cursojava.cap18_exceptions.checkedexception.test;

import cursojava.cap18_exceptions.checkedexception.classes.Leitor01;
import cursojava.cap18_exceptions.checkedexception.classes.Leitor02;

import java.io.*;

public class TryWithResourcesTest {
    public static void main(String[] args) {
        lerArquivoNew();
//        lerArquivoOld();
    }

    public static void lerArquivoNew() {
        try (Leitor01 leitor01 = new Leitor01(); Leitor02 leitor02 = new Leitor02()) {
//        try (Reader reader = new BufferedReader(new FileReader("teste.txt"))) {
            System.out.println("Executando o bloco try, usando um try with resources");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void lerArquivoOld() {
        Reader reader = null;
        try {
            reader = new BufferedReader(new FileReader("teste.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
