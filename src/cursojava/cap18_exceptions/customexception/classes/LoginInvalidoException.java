package cursojava.cap18_exceptions.customexception.classes;

public class LoginInvalidoException extends Exception {
    public LoginInvalidoException() {
        super("Usuário ou senha inválidos");
    }
}
