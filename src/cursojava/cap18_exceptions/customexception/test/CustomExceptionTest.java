package cursojava.cap18_exceptions.customexception.test;

import cursojava.cap18_exceptions.customexception.classes.LoginInvalidoException;

public class CustomExceptionTest {
    public static void main(String[] args) {
        try {
            logar();
        } catch (LoginInvalidoException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void logar() throws LoginInvalidoException {
        String usuarioBD = "Godofredo";
        String senhaBD = "123456";

        String usuarioDigitado = "Godofredo";
        String senhaDigitada = "123";


        if (!usuarioBD.equals(usuarioDigitado) || !senhaBD.equals(senhaDigitada)) {
            throw new LoginInvalidoException();
        } else {
            System.out.println("Acessando o seu perfil");
        }
    }
}

