package cursojava.cap18_exceptions.runtimeexception.test;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class MultipleRuntimeExceptionTest {
    public static void main(String[] args) {
        try {
//            throw new ArrayIndexOutOfBoundsException();
//            throw new IllegalArgumentException();
//            throw new ArithmeticException();
//            throw new RuntimeException();
            throw new IndexOutOfBoundsException();
//        } catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("Dentro do ArrayIndexOutOfBoundsException");
//        } catch (IllegalArgumentException e) {
//            System.out.println("Dentro do IllegalArgumentException");
//        } catch (ArithmeticException e) {
//            System.out.println("Dentro do ArithmeticException");
//        } catch (RuntimeException e) {
//            System.out.println("Dentro do RuntimeException");
//        }
        } catch (IllegalArgumentException | IndexOutOfBoundsException | ArithmeticException e) {
            System.out.println("Dentro de uma exception" + e.getMessage());
        }
        System.out.println("Fim do programa...");

        try {
            talvezLanceException();
        } catch (SQLException | AWTException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void talvezLanceException() throws SQLException, AWTException, IOException {

    }
}
