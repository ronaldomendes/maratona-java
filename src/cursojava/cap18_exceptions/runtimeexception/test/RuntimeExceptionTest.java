package cursojava.cap18_exceptions.runtimeexception.test;

public class RuntimeExceptionTest {
    public static void main(String[] args) {
        try {
            divisao(10, 0);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
//            e.printStackTrace();
        }

    }

    private static void divisao(int num1, int num2) {
        if (num2 == 0) {
            throw new IllegalArgumentException("O valor de \"num2\" deve ser diferente de ZERO");
        }
        int result = num1 / num2;
        System.out.println(result);
    }
}
