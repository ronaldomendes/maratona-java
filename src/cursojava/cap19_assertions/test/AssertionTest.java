package cursojava.cap19_assertions.test;

public class AssertionTest {
    public static void main(String[] args) {
//        calculaSalario(-2000);
//        calcSalario(-1000);
        diasDaSemana(10);
    }

    private static void calculaSalario(double salario) {
        assert (salario > 0) : "O salário deve ser maior do que ZERO";
        System.out.println("Calculando o seu salário...");
    }

    public static void calcSalario(double salario) {
        if (salario < 0) {
            throw new IllegalArgumentException();
        }
    }

    public static void diasDaSemana(int dia) {
        switch (dia) {
            case 1: case 7:
                System.out.println("Fim de semana");
                break;
            case 2: case 3: case 4: case 5: case 6:
                System.out.println("Dia útil");
                break;
            default:
                assert false;
                break;
        }
    }
}
