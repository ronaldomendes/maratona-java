package cursojava.cap20_wrappers.test;

public class WrappersTest {
    public static void main(String[] args) {
        byte bytePrimitivo = 1;
        short shortPrimitivo = 1;
        int intPrimitivo = 1;
        long longPrimitivo = 1L;
        float floatPrimitivo = 1f;
        double doublePrimitivo = 1d;
        char charPrimitivo = 'r';
        boolean boolPrimitivo = true;

        Byte byteWrapper = 1;
        Short shortWrapper = 1;
        Integer integerWrapper = 1;
        Long longWrapper = Long.valueOf("11");
        Float floatWrapper = 1f;
        Double doubleWrapper = 1d;
        Character characterWrapper = 'R';
        Boolean boolWrapper = true;

        String valor = "50";
        Float f = Float.parseFloat(valor);
        System.out.println(f);

        System.out.println(Character.isDigit('a'));
        System.out.println(Character.isLetter('a'));
        System.out.println(Character.isLetterOrDigit('@'));
    }
}
