package cursojava.cap21_strings.test;

public class StringBuilderTest {
    public static void main(String[] args) {
        String s = "Uma frase comum";
        StringBuilder sb = new StringBuilder(10);
        s.concat(" teste");
        sb.append("1, 2, 3... Testando");
        System.out.println(sb.reverse());
        System.out.println(sb.delete(0, 9));
        System.out.println(sb.insert(0, "Testando em "));
    }
}
