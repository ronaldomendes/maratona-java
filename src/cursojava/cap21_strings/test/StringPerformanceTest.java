package cursojava.cap21_strings.test;

public class StringPerformanceTest {
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        concatString(30000);
        long fim = System.currentTimeMillis();
        System.out.println("Tempo gasto concatenando Strings: " + (fim - inicio) + " ms");

        inicio = System.currentTimeMillis();
        concatStringBuilder(1000000);
        fim = System.currentTimeMillis();
        System.out.println("Tempo gasto com StringBuilder: " + (fim - inicio) + " ms");

        inicio = System.currentTimeMillis();
        concatStringBuffer(1000000);
        fim = System.currentTimeMillis();
        System.out.println("Tempo gasto com StringBuffer: " + (fim - inicio) + " ms");
    }

    private static void concatString(int tam) {
        String palavra = "";
        for (int i = 0; i < tam; i++) {
            palavra += i;
        }
    }

    private static void concatStringBuilder(int tam) {
        StringBuilder builder = new StringBuilder(tam);
        for (int i = 0; i < tam; i++) {
            builder.append(i);
        }
    }

    private static void concatStringBuffer(int tam) {
        StringBuffer buffer = new StringBuffer(tam);
        for (int i = 0; i < tam; i++) {
            buffer.append(i);
        }
    }
}
