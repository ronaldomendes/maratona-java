package cursojava.cap21_strings.test;

public class StringTest {
    public static void main(String[] args) {
        String nome = "Obi Wan";
        nome = nome.concat(" Kenobi");

        /*
        1- cria a variável de referência
        2- cria um objeto do tipo String
        3- cria uma string no pool de String
         */
        String nome2 = new String("Jabba the Hutt");

        String str01 = "Churrasco";
        System.out.println(str01.charAt(4));

        String str02 = "Cachorro Quente";
        System.out.println(str01.equalsIgnoreCase(str02));
        System.out.println(str01.length());
        System.out.println(str02.length());

        System.out.println(str02.replace("Quente", "Frio"));
        System.out.println(str01.substring(0, 7).toUpperCase());
    }
}
