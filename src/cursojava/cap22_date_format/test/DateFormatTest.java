package cursojava.cap22_date_format.test;

import java.text.DateFormat;
import java.util.Calendar;

public class DateFormatTest {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        DateFormat[] dateFormat = new DateFormat[6];
        dateFormat[0] = DateFormat.getInstance(); // 30/07/2020 14:15
        dateFormat[1] = DateFormat.getDateInstance(DateFormat.SHORT); // 30/07/2020
        dateFormat[2] = DateFormat.getDateInstance(DateFormat.MEDIUM); // 30 de jul de 2020
        dateFormat[3] = DateFormat.getDateInstance(DateFormat.LONG); // 30 de julho de 2020
        dateFormat[4] = DateFormat.getDateInstance(DateFormat.FULL); // quinta-feira, 30 de julho de 2020
        dateFormat[5] = DateFormat.getDateInstance(); // 30 de jul de 2020

        System.out.println(calendar);
        for (DateFormat date : dateFormat) {
            System.out.println(date.format(calendar.getTime()));
        }
    }
}
