package cursojava.cap22_date_format.test;

import java.util.Calendar;
import java.util.Date;

public class DateManiputationTest {
    public static void main(String[] args) {
        Date date = new Date();
        date.setTime(date.getTime() + 3_800_000L);
        System.out.println(date);

        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);

        if (Calendar.SUNDAY == calendar.getFirstDayOfWeek()) {
            System.out.println("Domingo é o primeiro dia da semana");
        }

//        System.out.println(calendar);
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        System.out.println(calendar.get(Calendar.DAY_OF_YEAR));
        System.out.println(calendar.get(Calendar.MONTH));

        calendar.add(Calendar.HOUR, 2); // aumenta horas
        calendar.add(Calendar.MONTH, 4); // aumenta meses
        calendar.roll(Calendar.MONTH, 4); // diminui meses

        Date newDate = calendar.getTime();
        System.out.println(newDate);
    }
}
