package cursojava.cap22_date_format.test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public class LocaleTest {
    public static void main(String[] args) {
        Locale japan = new Locale( "ja", "JP");
        Locale brazil = new Locale( "pt", "BR");
        Locale italy = new Locale( "it", "IT");
        Locale india = new Locale( "hi", "IN");
        Locale norway = new Locale("no", "NORWAY", "NY");

        Calendar calendar = Calendar.getInstance();
//        calendar.set(2016, Calendar.OCTOBER, 6);
        DateFormat dateFormat01 = DateFormat.getDateInstance(DateFormat.FULL, japan);
        DateFormat dateFormat02 = DateFormat.getDateInstance(DateFormat.FULL, brazil);
        DateFormat dateFormat03 = DateFormat.getDateInstance(DateFormat.FULL, italy);
        DateFormat dateFormat04 = DateFormat.getDateInstance(DateFormat.FULL, india);
        DateFormat dateFormat05 = DateFormat.getDateInstance(DateFormat.FULL, norway);

        System.out.println(dateFormat01.format(calendar.getTime()));
        System.out.println(dateFormat02.format(calendar.getTime()));
        System.out.println(dateFormat03.format(calendar.getTime()));
        System.out.println(dateFormat04.format(calendar.getTime()));
        System.out.println(dateFormat05.format(calendar.getTime()));

        System.out.println("===============================");
        System.out.println(japan.getDisplayCountry() + ", " + japan.getDisplayLanguage() + " - "
                + japan.getDisplayLanguage(japan) + ", " + japan.getLanguage() + ", " + japan.getCountry());
        System.out.println(brazil.getDisplayCountry() + ", " + brazil.getDisplayLanguage() + " - "
                + brazil.getDisplayLanguage(brazil) + ", " + brazil.getLanguage() + ", " + brazil.getCountry());
        System.out.println(italy.getDisplayCountry() + ", " + italy.getDisplayLanguage() + " - "
                + italy.getDisplayLanguage(italy) + ", " + italy.getLanguage() + ", " + italy.getCountry());
        System.out.println(india.getDisplayCountry() + ", " + india.getDisplayLanguage() + " - "
                + india.getDisplayLanguage(india) + ", " + india.getLanguage() + ", " + india.getCountry());
        System.out.println(norway.getDisplayCountry() + ", " + norway.getDisplayLanguage() + " - "
                + norway.getDisplayLanguage(norway) + ", " + norway.getLanguage() + ", " + norway.getCountry());
    }
}
