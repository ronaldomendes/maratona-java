package cursojava.cap22_date_format.test;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class NumberFormatTest {
    public static void main(String[] args) {
        float valor = 123.4567f;
        Locale japan = new Locale("ja", "JP");
        Locale france = new Locale("fr", "FR");

        NumberFormat[] numbers = new NumberFormat[6];
        numbers[0] = NumberFormat.getInstance();
        numbers[1] = NumberFormat.getInstance(japan);
        numbers[2] = NumberFormat.getInstance(france);
        numbers[3] = NumberFormat.getCurrencyInstance();
        numbers[4] = NumberFormat.getCurrencyInstance(france);
        numbers[5] = NumberFormat.getCurrencyInstance(japan);

        for (NumberFormat num: numbers) {
            System.out.println(num.format(valor));
        }

        NumberFormat nf = NumberFormat.getInstance();
        System.out.println(nf.getMaximumFractionDigits());
        nf.setMaximumFractionDigits(6);
        System.out.println(nf.getMaximumFractionDigits());
        System.out.println(nf.format(valor));

        String valorString  = "123.4567";
        try {
            System.out.println(nf.parse(valorString));
            nf.setParseIntegerOnly(true);
            System.out.println(nf.parse(valorString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
