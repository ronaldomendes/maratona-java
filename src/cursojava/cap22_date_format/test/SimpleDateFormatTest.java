package cursojava.cap22_date_format.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SimpleDateFormatTest {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        String mask01 = "yyyy/MM/dd G 'at' HH:mm:ss z";
        String mask02 = "'São Paulo,' dd 'de' MMMM 'de' yyyy zzzz HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(mask02);

        System.out.println(sdf.format(calendar.getTime()));
    }
}
