package cursojava.cap23_regex.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressoesRegularesTest {
    public static void main(String[] args) {

        /*
        \d - procura todos os dígitos
        \D - procura tudo o que não for dígito
        \s - espaços em branco \t \n \f \r
        \S - procura caractere que não é branco
        \w - procura por letras a-z, A-Z, 0-9 e _
        \W - procura por caracteres especiais

        quantificadores de ocorrências
        ? zero ou uma vez
        * zero ou mais vezes
        + uma ou mais vezes
        {n, m} de n até m vezes
        ( ) agrupa uma expressão
        | ou
        $ fim de linha
         */
//        String regexHex = "0[xX]([0-9a-fA-F])+(\\s|$)";
//        String textoHex = "12 0x 0x 0x01FFABC 0x10G 0x1";

        String regexEmail01 = "([a-zA-Z0-9\\._-])+@([a-zA-Z])+(\\.([a-zA-Z])+)+";
        String regexEmail02 = "([\\w\\._-])+@([a-zA-Z])+(\\.([a-zA-Z])+)+";
        String textoEmail = "mendes-ronaldo@live.com, 123abc@mail, fulano@gmail.br, mendesh.ronaldo@gmail.com, teste@mail, godo.fredo@email.com.br";

//        System.out.println("E-mail válido? " + "#@!teste@email.com".matches(regexEmail02));

//        String regexData = "\\d{2}/\\d{2}/\\d{2,4}";
//        String textoData = "09/05/2020 19/06/1987 06/10/2016 11/09/2001 1/1/11 01/05/94";

        String regexFile = "proj([^,])*";
        String textoFile = "proj1.bkp, proj1.java, proj1.class, proj1final.java, proj2.bkp, proj3.java";

        Pattern pattern = Pattern.compile(regexFile);
        Matcher matcher = pattern.matcher(textoFile);
        System.out.println("Texto: " + textoFile);
        System.out.println("Índice: ");
        System.out.println("RegEx: " + matcher.pattern());
        System.out.println("Posições encontradas");

        while (matcher.find()) {
            System.out.println(matcher.start() + " " + matcher.group());
        }
    }
}
