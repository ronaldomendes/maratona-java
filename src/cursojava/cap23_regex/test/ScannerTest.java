package cursojava.cap23_regex.test;

import java.util.Scanner;

public class ScannerTest {
    public static void main(String[] args) {
        Scanner scanner01 = new Scanner("1 true 100 olá");
        while (scanner01.hasNext()) {
            System.out.println(scanner01.next());
        }

        System.out.println("\n#################\n");

        Scanner scanner02 = new Scanner("1 true 100 olá");
        while (scanner02.hasNext()) {
            if (scanner02.hasNextInt()) {
                int i = scanner02.nextInt();
                System.out.println("Tipo int: " + i);
            } else if (scanner02.hasNextBoolean()) {
                boolean b = scanner02.nextBoolean();
                System.out.println("Tipo boolean: " + b);
            } else {
                System.out.println("Tipo String: " + scanner02.next());
            }
        }



    }
}
