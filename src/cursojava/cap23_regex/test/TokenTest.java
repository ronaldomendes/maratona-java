package cursojava.cap23_regex.test;

public class TokenTest {
    public static void main(String[] args) {
        String str = "Willian, Paulo, Joana, Camila, José, Mateus, Godofredo, Creuza, Ronaldo";
        String[] tokens = str.split(",");

        for(String token: tokens) {
            System.out.println(token.trim());
        }
    }
}
