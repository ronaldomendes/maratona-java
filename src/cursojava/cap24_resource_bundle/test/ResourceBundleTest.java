package cursojava.cap24_resource_bundle.test;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleTest {
    public static void main(String[] args) {
//        System.out.println(Locale.getDefault());
        ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("en", "US"));
        System.out.println(bundle.getString("hello"));
        System.out.println(bundle.getString("good.morning"));
        System.out.println(bundle.getString("upload"));

        bundle = ResourceBundle.getBundle("messages", new Locale("pt", "BR"));
        System.out.println(bundle.getString("hello"));
        System.out.println(bundle.getString("good.morning"));
        System.out.println(bundle.getString("download"));

        /*
        Exemplo: Procurando um arquivo com as mensagens em francês do Canadá
        Locale locale = new Locale("fr", "CA");
        ResourceBundle.getBundle("messages", locale);

        Ordem de busca pelo arquivo de configuração
        1- messages_fr_CA.properties (busca a correspondência exata do arquivo)
        2- messages_fr.properties

        3- messages_pt_BR.properties (busca no arquivo com a configuração de padrão)
        4- messages_pt.properties

        5- messages.properties (Procura o arquivo base)
        6- MissingResourceException (Caso não encontra, lança uma exception)
        */
    }
}
