package cursojava.cap25_input_output.test;

import java.io.*;

public class BufferedTest {
    public static void main(String[] args) {

        File file = new File("Arquivo.txt");

//        Modo 1 - Try/Catch
//        try {
//            FileWriter fwriter = new FileWriter(file);
//            BufferedWriter bwriter = new BufferedWriter(fwriter);
//
//            bwriter.write("Lorem ipsum rutrum etiam felis vulputate tortor nisl mauris,");
//            bwriter.newLine();
//            bwriter.write("gravida vel blandit etiam interdum semper.");
//            bwriter.newLine();
//            bwriter.write("Um hamburguer e um pastel, por favor! XD");
//            bwriter.flush();
//            bwriter.close();
//
////            lendo um arquivo
//            FileReader freader = new FileReader(file);
//            BufferedReader breader = new BufferedReader(freader);
//
//            String s;
//            while ((s = breader.readLine()) != null) {
//                System.out.println(s);
//            }
//            breader.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        Modo 2 - Try with Resources
        try (BufferedWriter bwriter = new BufferedWriter(new FileWriter(file));
             BufferedReader breader = new BufferedReader(new FileReader(file))) {
            bwriter.write("Lorem ipsum rutrum etiam felis vulputate tortor nisl mauris,");
            bwriter.newLine();
            bwriter.write("gravida vel blandit etiam interdum semper.");
            bwriter.newLine();
            bwriter.write("Um hamburguer e um pastel, por favor! XD");
            bwriter.flush();
            bwriter.close();

            String s;
            while ((s = breader.readLine()) != null) {
                System.out.println(s);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
