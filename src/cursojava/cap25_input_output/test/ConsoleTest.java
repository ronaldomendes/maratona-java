package cursojava.cap25_input_output.test;

import java.io.Console;

public class ConsoleTest {
    public static void main(String[] args) {
        Console console = System.console();
        char[] pw = console.readPassword("%s", "pw: ");

        for(char c: pw) {
            console.format("%c ", c);
        }
        console.format("\n");

        String texto;
        while (true) {
            texto = console.readLine("%s", "Digite: ");
            console.format("Resultado: %s", retorno(texto));
        }
    }

    public static String retorno(String arg) {
        return arg;
    }
}
