package cursojava.cap25_input_output.test;

import java.io.File;
import java.io.IOException;

public class FileDiretorioTest {
    public static void main(String[] args) {
//        File folder = new File("myFolder");
//        boolean mkdir = folder.mkdir();
//        System.out.println("Diretório criado? " + mkdir);

//        Modo 01
//        String pathName = "C:\\Users\\Ronaldo Mendes\\Documents\\NetBeansProjects\\maratona-java\\myFolder";
//        File file = new File(pathName + "\\arquivo.txt");

//        Modo 02
//        File file = new File(folder, "arquivo.txt");
//        try {
//            boolean newFile = file.createNewFile();
//            System.out.println("Arquivo criado? " + newFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        File newFileName = new File(folder, "arquivo_renomeado.txt");
//        boolean newName = file.renameTo(newFileName);
//        System.out.println("Arquivo renomeado? " + newName);
//
//        File newFolderName = new File("newFolder");
//        boolean folderName = folder.renameTo(newFolderName);
//        System.out.println("Diretório renomeado? " + folderName);

        buscarArquivos();
    }

    public static void buscarArquivos() {
        String path = "C:\\Users\\Ronaldo Mendes\\Documents\\NetBeansProjects\\maratona-java\\src\\cursojava\\cap22_date_format\\test";
        File file = new File("newFolder");
        String[] list = file.list();

        for (String arquivo : list) {
            System.out.println(arquivo);
        }
    }
}
