package cursojava.cap25_input_output.test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class FileTest {
    public static void main(String[] args) {
        File file = new File("Arquivo.txt");

        try {
            System.out.println(file.createNewFile());
            boolean fileExists = file.exists();
            System.out.println("Permissão de leitura? " + file.canRead());
            System.out.println("Path relativo: " + file.getPath());
            System.out.println("Path absoluto: " + file.getAbsolutePath());
            System.out.println("Diretório? " + file.isDirectory());
            System.out.println("Hidden? " + file.isHidden());
            System.out.println("Last modified? " + new Date(file.lastModified()));
            if (fileExists) {
                System.out.println("Deletado? " + file.delete());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
