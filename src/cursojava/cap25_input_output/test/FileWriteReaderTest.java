package cursojava.cap25_input_output.test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriteReaderTest {
    public static void main(String[] args) {
        File file = new File("Arquivo.txt");
        String msg = "Lorem ipsum rutrum etiam felis vulputate tortor nisl mauris," +
                "\ngravida vel blandit etiam interdum semper.";

//        Modo 1 - Try/Catch
//        try {
//            escrevendo um arquivo (se eu usar FileWriter(file, true) o conteúdo do arquivo será concatenado)
//            FileWriter writer = new FileWriter(file);
//            writer.write(msg);
//            writer.flush();
//            writer.close();
//
////            lendo um arquivo
//            FileReader reader = new FileReader(file);
//            char[] chars = new char[500];
//            int size = reader.read(chars);
//            System.out.println("Tamanho: " + size);
//
//            for (char c: chars) {
//                System.out.print(c);
//            }
//            reader.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        Modo 2 - Try with Resources
        try (FileWriter writer = new FileWriter(file); FileReader reader = new FileReader(file)) {
            writer.write(msg);
            writer.flush();
            char[] chars = new char[500];
            int size = reader.read(chars);
            System.out.println("Tamanho: " + size);

            for (char c : chars) {
                System.out.print(c);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
