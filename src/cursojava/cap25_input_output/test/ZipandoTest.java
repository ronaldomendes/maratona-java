package cursojava.cap25_input_output.test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipandoTest {
    public static void main(String[] args) {
        // caminho onde o arquivo zipado será salvo
        Path dirZip = Paths.get("pasta01");
        // caminho dos arquivos que serão compactados
        Path dirFiles = Paths.get("pasta02/subpasta01/subpasta02");
        // nome do arquivo zip
        Path zipFile = dirZip.resolve("arquivo.zip");

        /*
        O método abaixo irá identificar e ler todos os arquivos antes de criar o zip
         e ao término irá salvar no diretório configurado
         */
        try (ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(zipFile.toFile()));
             DirectoryStream<Path> stream = Files.newDirectoryStream(dirFiles)) {
            for (Path path : stream) {
                System.out.println(path);
                ZipEntry entry = new ZipEntry(path.getFileName().toString());
                zip.putNextEntry(entry);

                // Modo 01
//                FileInputStream inputStream = new FileInputStream(path.toFile());
//                BufferedInputStream bufferedStream = new BufferedInputStream(inputStream);

                // Modo 02
                BufferedInputStream bufferedStream = new BufferedInputStream(new FileInputStream(path.toFile()));
                byte[] buff = new byte[2048];
                int bytesRead;
                while ((bytesRead = bufferedStream.read(buff)) > 0) {
                    zip.write(buff, 0, bytesRead);
                }
                zip.flush();
                zip.closeEntry();
                bufferedStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
