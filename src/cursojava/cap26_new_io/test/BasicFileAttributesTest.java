package cursojava.cap26_new_io.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class BasicFileAttributesTest {
    public static void main(String[] args) throws IOException {
        Date primeiroDeDezembro = new GregorianCalendar(2015, Calendar.DECEMBER, 1).getTime();
        File file = new File("newFolder\\arquivo.txt");
        file.createNewFile();
        file.setLastModified(primeiroDeDezembro.getTime());
        System.out.println(file.lastModified());
        file.delete();

        Path path = Paths.get("newFolder\\arquivo_path.txt");
        Files.createFile(path);
        FileTime fileTime = FileTime.fromMillis(primeiroDeDezembro.getTime());
        Files.setLastModifiedTime(path, fileTime);
        System.out.println(Files.getLastModifiedTime(path));
        Files.deleteIfExists(path);

        path = Paths.get("src\\cursojava\\cap05_classes\\entity\\Carro.java");
        System.out.println(Files.isReadable(path));
        System.out.println(Files.isWritable(path));
        System.out.println(Files.isExecutable(path));

//        BasicFileAttributes, PosixFileAttributes, DosFileAttributes
        BasicFileAttributes atributosBasicos = Files.readAttributes(path, BasicFileAttributes.class);
        System.out.println(atributosBasicos.creationTime());
        System.out.println(atributosBasicos.lastAccessTime());
        System.out.println(atributosBasicos.lastModifiedTime());
        System.out.println(atributosBasicos.isDirectory());
        System.out.println(atributosBasicos.isSymbolicLink());
        System.out.println(atributosBasicos.isRegularFile());
        System.out.println("\n=================================\n");

//        BasicFileAttributesView, PosixFileAttributesView, DosFileAttributesView, FileOwnerAttributeView, AclFileAttributeView
        FileTime lastModified = atributosBasicos.lastModifiedTime();
        FileTime lastAccess = FileTime.fromMillis(System.currentTimeMillis());
        FileTime createdAt = atributosBasicos.creationTime();
        BasicFileAttributeView basicView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        basicView.setTimes(lastModified, lastAccess, createdAt);
        atributosBasicos = Files.readAttributes(path, BasicFileAttributes.class);
        System.out.println(atributosBasicos.creationTime());
        System.out.println(atributosBasicos.lastAccessTime());
        System.out.println(atributosBasicos.lastModifiedTime());
    }
}
