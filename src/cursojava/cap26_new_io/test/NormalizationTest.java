package cursojava.cap26_new_io.test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NormalizationTest {
    public static void main(String[] args) {
        String diretorioProjeto = "Url_principal\\pasta01\\pasta02\\pasta03";
        String arquivoTxt = "..\\..\\arquivo.txt";
        Path path01 = Paths.get(diretorioProjeto, arquivoTxt);
        System.out.println(path01);
        System.out.println(path01.normalize());

        Path path02 = Paths.get("home/./godofredo/./dev");
        System.out.println(path02);
        System.out.println(path02.normalize());
    }
}
