package cursojava.cap26_new_io.test;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

class FindAllTest extends SimpleFileVisitor<Path> {
    private PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*{Test*}.{java,class}");

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (matcher.matches(file)) {
            System.out.println(file.getFileName());
        }
        return FileVisitResult.CONTINUE;
    }
}

public class PathMatcherTest {
    public static void main(String[] args) throws IOException {
        Path path01 = Paths.get("pasta02/subpasta01/subpasta02/file.bkp");
        Path path02 = Paths.get("file.bkp");
        Path path03 = Paths.get("maratona-java");

        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.bkp");
        System.out.println(matcher.matches(path01));
        System.out.println(matcher.matches(path02));

        System.out.println("\n=======================\n");
        matches(path01, "glob:*.bkp");
        matches(path01, "glob:**.bkp");
        matches(path01, "glob:*");
        matches(path01, "glob:**");

        System.out.println("\n=======================\n");
        matches(path01, "glob:*.???");
        matches(path01, "glob:**/*.???");
        matches(path01, "glob:**.???");

        System.out.println("\n=======================\n");
        matches(path03, "glob:{maratona*,java*}");
        matches(path03, "glob:{maratona,java}*");
        matches(path03, "glob:{maratona,java}");

        Files.walkFileTree(Paths.get("./") , new FindAllTest());
    }

    private static void matches(Path path, String glob) {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(glob);
        System.out.println(String.format("%s: %s", glob, matcher.matches(path)));
    }
}
