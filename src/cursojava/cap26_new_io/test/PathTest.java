package cursojava.cap26_new_io.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class PathTest {
    public static void main(String[] args) {
        Path path01 = Paths.get("C:\\Users\\Ronaldo Mendes\\Documents\\NetBeansProjects\\maratona-java\\Arquivo.txt");
        Path path02 = Paths.get("C:\\Users\\Ronaldo Mendes\\Documents\\NetBeansProjects\\maratona-java", "Arquivo.txt");
        Path path03 = Paths.get("C:", "Users\\Ronaldo Mendes\\Documents\\NetBeansProjects\\maratona-java", "Arquivo.txt");
        Path path04 = Paths.get("C:", "Users", "Ronaldo Mendes", "Documents", "NetBeansProjects", "maratona-java", "Arquivo.txt");

//        System.out.println(path04.toAbsolutePath());
        File file = path04.toFile();
        Path path = file.toPath();

        Path path05 = Paths.get("pasta01");
        try {
            if (Files.notExists(path05)) Files.createDirectory(path05);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path path06 = Paths.get("pasta02\\subpasta01\\subpasta02");
        Path arquivo = Paths.get("pasta02\\subpasta01\\subpasta02\\file.txt");
        try {
            if (Files.notExists(path06)) {
                Files.createDirectories(path06);
            }
            Files.deleteIfExists(arquivo);
            Files.createFile(arquivo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path path07 = Paths.get("pasta03\\subpasta01\\subpasta02\\file.txt");
        try {
            if (Files.notExists(path07.getParent())) {
                Files.createDirectories(path07.getParent());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path source = Paths.get("newFolder\\arquivo_renomeado.txt");
        Path target = Paths.get(path05.toString() + "\\arquivo_copiado.txt");
        try {
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
