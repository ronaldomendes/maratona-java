package cursojava.cap26_new_io.test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class RelativizarTest {
    public static void main(String[] args) {
        Path dir = Paths.get("/home/godofredo");
        Path classe = Paths.get("/home/godofredo/java/Pessoa.java");
        Path pathToClasse = dir.relativize(classe);
        System.out.println(pathToClasse);

        Path absoluto01 = Paths.get("/home/godofredo");
        Path absoluto02 = Paths.get("/usr/local");
        Path absoluto03 = Paths.get("/home/godofredo/java/Pessoa.java");

        Path relativo01 = Paths.get("temp");
        Path relativo02 = Paths.get("temp/Funcionario.java");

        System.out.println("1: " + absoluto01.relativize(absoluto03));
        System.out.println("2: " + absoluto03.relativize(absoluto01));
        System.out.println("3: " + absoluto01.relativize(absoluto02));
        System.out.println("4: " + relativo01.relativize(relativo02));
        System.out.println("5: " + relativo02.relativize(relativo01));
    }
}
