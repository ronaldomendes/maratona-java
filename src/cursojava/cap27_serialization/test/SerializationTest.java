package cursojava.cap27_serialization.test;

import cursojava.cap27_serialization.classes.Aluno;
import cursojava.cap27_serialization.classes.Turma;

import java.io.*;

public class SerializationTest {
    public static void main(String[] args) {
        gravadorObjeto();
        leitorObjeto();
    }

    private static void gravadorObjeto() {
        Turma turma = new Turma("Maratona Java, apenas para os brabos!");
        Aluno aluno = new Aluno(1L, "Godofredo", "Senha123");
        aluno.setTurma(turma);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("aluno.ser"))) {
            objectOutputStream.writeObject(aluno);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void leitorObjeto() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("aluno.ser"))) {
            Aluno aluno = (Aluno) objectInputStream.readObject();
            System.out.println(aluno);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
