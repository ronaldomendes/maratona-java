package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Produto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BinarySearchTest {
    public static void main(String[] args) {
        List<Integer> nums = new ArrayList<>();
        nums.addAll(Arrays.asList(2, 56, 8, 0, 5, 900, 45, 6, 78, 34, 24, 56, 78, 734, 534, 67, 567, 895, 786));

        Collections.sort(nums);
        System.out.println(Collections.binarySearch(nums, 900));

        List<Produto> produtos = new ArrayList<>();

        Produto prod1 = new Produto("123", "Notebook Posilixo", 1999.99);
        Produto prod2 = new Produto("680", "TV Tubo Telefunken", 399.99);
        Produto prod3 = new Produto("124", "Smartphone Xing Ling", 799.99);
        Produto prod4 = new Produto("910", "Rádio Portátil Motoradio", 99.99);
        produtos.addAll(Arrays.asList(prod1, prod2, prod3, prod4));

        Collections.sort(produtos, new ProdutoNomeComparator());
        Produto prod5 = new Produto("000", "Kit Facas Ginsu", 299.99);
        System.out.println(Collections.binarySearch(produtos, prod5, new ProdutoNomeComparator()));
        for (Produto prod : produtos) {
            System.out.println(prod);
        }

        Integer[] arrayInteger = new Integer[4];
        arrayInteger[0] = 5;
        arrayInteger[1] = 8;
        arrayInteger[2] = 2;
        arrayInteger[3] = 0;
        Arrays.sort(arrayInteger);
        System.out.println(Arrays.binarySearch(arrayInteger, 1));
    }
}
