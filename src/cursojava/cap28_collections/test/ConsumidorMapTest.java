package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Consumidor;
import cursojava.cap28_collections.classes.Produto;

import java.util.*;

public class ConsumidorMapTest {
    public static void main(String[] args) {
        Consumidor cons01 = new Consumidor("Godofredo Silva", "321.654.851-50");
        Consumidor cons02 = new Consumidor("Indústria Tabajara", "65.650.210.0001-40");

        Produto prod1 = new Produto("123", "Notebook Posilixo", 1999.99);
        Produto prod2 = new Produto("680", "TV Tubo Telefunken", 399.99);
        Produto prod3 = new Produto("124", "Smartphone Xing Ling", 799.99);
        Produto prod4 = new Produto("910", "Rádio Portátil Motoradio", 99.99);

//        Map<Consumidor, Produto> map = new HashMap<>();
//        map.put(cons01, prod2);
//        map.put(cons02, prod1);
//        map.put(cons02, prod3);
//        map.put(cons01, prod4);

        List<Produto> prods01 = new ArrayList<>(Arrays.asList(prod1, prod2));
        List<Produto> prods02 = new ArrayList<>(Arrays.asList(prod3, prod4));

//        for (Map.Entry<Consumidor, Produto> keyValue : map.entrySet()) {
//            System.out.println(keyValue);
//            System.out.println(keyValue.getKey().getNome() + " - " + keyValue.getValue().getNome());
//        }

        Map<Consumidor, List<Produto>> mapList = new HashMap<>();
        mapList.put(cons01, prods02);
        mapList.put(cons02, prods01);

        for (Map.Entry<Consumidor, List<Produto>> values : mapList.entrySet()) {
            System.out.println(values.getKey().getNome());
            for (Produto prod : values.getValue()) {
                System.out.println("\t" + prod.getNome());
            }
        }
    }
}
