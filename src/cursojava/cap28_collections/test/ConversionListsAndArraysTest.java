package cursojava.cap28_collections.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConversionListsAndArraysTest {
    public static void main(String[] args) {
        List<Integer> nums = new ArrayList<>();
        nums.addAll(Arrays.asList(2, 4, 7, 9));

        Integer[] arrayNums = new Integer[nums.size()];
        nums.toArray(arrayNums);
        System.out.println(nums);
        System.out.println(Arrays.toString(arrayNums));

        Integer[] arraysNums2 = new Integer[4];
        arraysNums2[0] = 10;
        arraysNums2[1] = 9;
        arraysNums2[2] = 8;
        arraysNums2[3] = 7;
        List<Integer> nums2 = Arrays.asList(arraysNums2);

        nums2.set(0, 6);
        List<Integer> nums3 = new ArrayList<>();
        nums3.addAll(Arrays.asList(arraysNums2));

        nums3.add(10);
        System.out.println(nums2);
        System.out.println(nums3);
        System.out.println(Arrays.toString(arraysNums2));

    }
}
