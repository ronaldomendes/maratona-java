package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Celular;

public class EqualsTest {
    public static void main(String[] args) {
        String nome1 = "Godofredo Silva";
        String nome2 = new String("Godofredo Silva");
        Integer int1 = 5;
//        Integer int2 = new Integer(5);
        System.out.println(nome1.equals(nome2));
//        System.out.println(int1.equals(int2));

        Celular c1 = new Celular("iPim", "12345");
        Celular c2 = new Celular("iPim", "12345");
        System.out.println(c1.equals(c2));
        System.out.println(c2.equals(c1));
    }
}
