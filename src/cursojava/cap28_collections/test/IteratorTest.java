package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Produto;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class IteratorTest {
    public static void main(String[] args) {
        List<Produto> produtos = new LinkedList<>();

        Produto prod1 = new Produto("123", "Notebook Posilixo", 1999.99, 0);
        Produto prod2 = new Produto("680", "TV Tubo Telefunken", 399.99, 10);
        Produto prod3 = new Produto("124", "Smartphone Xing Ling", 799.99, 5);
        Produto prod4 = new Produto("910", "Rádio Portátil Motoradio", 99.99, 0);
        produtos.addAll(Arrays.asList(prod1, prod2, prod3, prod4));

        Iterator<Produto> produtoIterator = produtos.iterator();
        while (produtoIterator.hasNext()) {
            if (produtoIterator.next().getQuantidade() == 0) {
                produtoIterator.remove();
            }
        }

        System.out.println(produtos.size());
    }
}
