package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Celular;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListCelularTest {
    public static void main(String[] args) {
        Celular celular1 = new Celular("Xing Ling", "123456");
        Celular celular2 = new Celular("iPim", "650840");
        Celular celular3 = new Celular("Nokia Tijolão", "380795");

        List<Celular> list = new ArrayList<>();
        list.addAll(Arrays.asList(celular1, celular2, celular3));

        list.forEach(System.out::println);

        Celular celular4 = new Celular("iPim", "650840");
        System.out.println(celular2 == celular4);
        System.out.println(celular2.equals(celular4));
        System.out.println(list.contains(celular4));
    }
}
