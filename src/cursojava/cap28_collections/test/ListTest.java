package cursojava.cap28_collections.test;

import java.util.ArrayList;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {
        List<String> nomes = new ArrayList<>();
        List<String> nomes2 = new ArrayList<>();
        nomes.add("Godofredo");
        nomes.add("Creuza");
        nomes.add("Tonho");
        nomes2.add("Tião");
        nomes2.add("Zezinho");

        for (int i = 0; i < nomes.size(); i++) {
            System.out.println(nomes.get(i));
        }

        nomes.remove("Creuza");

        for (String nome : nomes) {
            System.out.println(nome);
        }

        nomes.clear();
        nomes.addAll(nomes2);
        //        Modo 02 - com Java 8
        nomes.forEach(System.out::println);

        List<Integer> nums = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            nums.add(i);
        }
        for (Integer num : nums) {
            System.out.print(num + " ");
        }
    }
}
