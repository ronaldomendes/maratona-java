package cursojava.cap28_collections.test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("teklado", "teclado");
        hashMap.put("mauze", "mouse");
        hashMap.put("vc", "você");
        hashMap.put("meza", "mesa");
        hashMap.put("kaza", "casa");

        System.out.println("\nO HashMap não mostra na ordem que o item é inserido");
        System.out.println("*Chave");
        for (String key : hashMap.keySet()) {
            System.out.println(key);
        }

        System.out.println("**Valor");
        for (String value : hashMap.values()) {
            System.out.println(value);
        }

        System.out.println("***Chave e Valor");
        for (Map.Entry<String, String> keyValue : hashMap.entrySet()) {
            System.out.println(keyValue);
        }

        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("teklado", "teclado");
        linkedHashMap.put("mauze", "mouse");
        linkedHashMap.put("vc", "você");
        linkedHashMap.put("meza", "mesa");
        linkedHashMap.put("kaza", "casa");

        System.out.println("\nO LinkedHashMap mostra na ordem que o item é inserido");

        for (Map.Entry<String, String> keyValue : linkedHashMap.entrySet()) {
            System.out.println(keyValue);
        }
    }
}
