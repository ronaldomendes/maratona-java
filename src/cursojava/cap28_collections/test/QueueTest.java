package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Consumidor;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueTest {
    public static void main(String[] args) {
        Queue<String> stringQueue = new PriorityQueue<>();
        stringQueue.add("C");
        stringQueue.add("D");
        stringQueue.add("A");
        stringQueue.add("B");
        System.out.println(stringQueue);
        System.out.println(stringQueue.peek());
        System.out.println(stringQueue.size());

        System.out.println(stringQueue.poll());
        System.out.println(stringQueue.size());

        System.out.println(stringQueue.remove());
        System.out.println(stringQueue.size());

        System.out.println(stringQueue.remove("C"));
        System.out.println(stringQueue.size());


        Queue<Consumidor> consumidorQueue = new PriorityQueue<>();
        Consumidor consumidor = new Consumidor("Creuza Silva", "321");
//        consumidorQueue.add(consumidor);
//        System.out.println(consumidorQueue);
    }
}
