package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Produto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class SetTest {
    public static void main(String[] args) {
        Produto prod1 = new Produto("123", "Notebook Posilixo", 1999.99, 0);
        Produto prod2 = new Produto("680", "TV Tubo Telefunken", 399.99, 10);
        Produto prod3 = new Produto("124", "Smartphone Xing Ling", 799.99, 5);
        Produto prod4 = new Produto("910", "Rádio Portátil Motoradio", 99.99, 0);
        Produto prod5 = new Produto("310", "Videogame Telejogo", 499.99, 2);

        System.out.println("\nO HashSet não mostra na ordem que o item é inserido");
        Set<Produto> produtoHashSet = new HashSet<>();
        produtoHashSet.addAll(Arrays.asList(prod1, prod2, prod3, prod4, prod5));

        for (Produto prod : produtoHashSet) {
            System.out.println(prod);
        }

        System.out.println("\nO LinkedHashSet mostra na ordem que o item é inserido");
        Set<Produto> produtoLinkedHashSet = new LinkedHashSet<>();
        produtoLinkedHashSet.addAll(Arrays.asList(prod1, prod2, prod3, prod4, prod5));

        for (Produto prod : produtoLinkedHashSet) {
            System.out.println(prod);
        }
    }
}
