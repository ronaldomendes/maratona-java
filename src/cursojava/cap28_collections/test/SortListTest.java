package cursojava.cap28_collections.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortListTest {
    public static void main(String[] args) {
        List<String> nomes = new ArrayList<>();
        nomes.add("Godofredo");
        nomes.add("Ronaldo");
        nomes.add("Creuza");
        nomes.add("Zezinho");
        nomes.add(0, "Tião");
        nomes.add("Raimunda");

        Collections.sort(nomes);
        for (String nome : nomes) {
            System.out.println(nome);
        }

        List<Double> nums = new ArrayList<>();
        nums.add(29.5);
        nums.add(8.4);
        nums.add(2.9);

        Collections.sort(nums);
        for (Double num : nums) {
            System.out.println(num);
        }
    }
}
