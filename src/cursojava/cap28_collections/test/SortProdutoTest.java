package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Produto;

import java.util.*;

class ProdutoNomeComparator implements Comparator<Produto> {

    @Override
    public int compare(Produto o1, Produto o2) {
        return o1.getNome().compareTo(o2.getNome());
    }
}

class ProdutoSerialNumberComparator implements Comparator<Produto> {

    @Override
    public int compare(Produto o1, Produto o2) {
        return o1.getSerialNumber().compareTo(o2.getSerialNumber());
    }
}

public class SortProdutoTest {
    public static void main(String[] args) {
        List<Produto> produtos = new ArrayList<>();

        Produto prod1 = new Produto("123", "Notebook Posilixo", 1999.99);
        Produto prod2 = new Produto("680", "TV Tubo Telefunken", 399.99);
        Produto prod3 = new Produto("124", "Smartphone Xing Ling", 799.99);
        Produto prod4 = new Produto("910", "Rádio Portátil Motoradio", 99.99);

        Produto[] arrayProdutos = new Produto[4];
        arrayProdutos[0] = prod1;
        arrayProdutos[1] = prod2;
        arrayProdutos[2] = prod3;
        arrayProdutos[3] = prod4;

        produtos.addAll(Arrays.asList(prod1, prod2, prod3, prod4));
        Collections.sort(produtos, new ProdutoNomeComparator());

        System.out.println("\nImprimindo um list");
        for (Produto prod : produtos) {
            System.out.println(prod);
        }

        System.out.println("\nImprimindo um array");
        Arrays.sort(arrayProdutos, new ProdutoSerialNumberComparator());
//        System.out.println(Arrays.toString(arrayProdutos));
        for (Produto prod : arrayProdutos) {
            System.out.println(prod);
        }
    }
}
