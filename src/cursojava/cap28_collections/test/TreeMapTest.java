package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Consumidor;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class TreeMapTest {
    public static void main(String[] args) {
        NavigableMap<String, String> map = new TreeMap<>();
        map.put("A", "Letra A");
        map.put("D", "Letra D");
        map.put("C", "Letra C");
        map.put("B", "Letra B");

        NavigableMap<String, Consumidor> map2 = new TreeMap<>();
        Consumidor cons01 = new Consumidor("Godofredo Silva", "321.654.851-50");
        Consumidor cons02 = new Consumidor("Indústria Tabajara", "65.650.210.0001-40");
        map2.put("E", cons01);
        map2.put("A", cons02);

        for (Map.Entry<String, String> keyValue : map.entrySet()) {
            System.out.println(keyValue);
        }

        for (Map.Entry<String, Consumidor> keyValue : map2.entrySet()) {
            System.out.println(keyValue);
        }

        System.out.println(map.headMap("C"));
        System.out.println(map.ceilingEntry("C"));
        System.out.println(map.higherEntry("B"));
        System.out.println(map.descendingMap());
    }
}
