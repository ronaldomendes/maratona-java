package cursojava.cap28_collections.test;

import cursojava.cap28_collections.classes.Celular;
import cursojava.cap28_collections.classes.Produto;

import java.util.Arrays;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;


class CelularNomeComparator implements Comparator<Celular> {

    @Override
    public int compare(Celular o1, Celular o2) {
        return o1.getNome().compareTo(o2.getNome());
    }
}

public class TreeSetTest {
    public static void main(String[] args) {
        Produto prod1 = new Produto("123", "Notebook Posilixo", 1999.99, 0);
        Produto prod2 = new Produto("680", "TV Tubo Telefunken", 399.99, 10);
        Produto prod3 = new Produto("124", "Smartphone Xing Ling", 799.99, 5);
        Produto prod4 = new Produto("910", "Rádio Portátil Motoradio", 99.99, 0);
        Produto prod5 = new Produto("310", "Videogame Telejogo", 499.99, 2);

        NavigableSet<Produto> produtoNavigableSet = new TreeSet<>();
        produtoNavigableSet.addAll(Arrays.asList(prod1, prod2, prod3, prod4, prod5));
        for (Produto prod : produtoNavigableSet) {
            System.out.println(prod);
        }

        Celular celular = new Celular("iPim", "56407");
        NavigableSet<Celular> celularNavigableSet = new TreeSet<>(new CelularNomeComparator());
        celularNavigableSet.add(celular);

        for (Celular cel : celularNavigableSet) {
            System.out.println(cel);
        }

        System.out.println("\n======================\n");
        System.out.println(produtoNavigableSet.lower(prod3));
        System.out.println(produtoNavigableSet.floor(prod3));
        System.out.println(produtoNavigableSet.higher(prod3));
        System.out.println(produtoNavigableSet.ceiling(prod3));

        System.out.println("\n======================\n");
        for (Produto prod : produtoNavigableSet.descendingSet()) {
            System.out.println(prod);
        }

        System.out.println("Tamanho original da lista: " + produtoNavigableSet.size());
        System.out.println(produtoNavigableSet.pollFirst());
        System.out.println("Após remover o primeiro da lista: " + produtoNavigableSet.size());
        System.out.println(produtoNavigableSet.pollLast());
        System.out.println("Após remover o último da lista: " + produtoNavigableSet.size());
    }
}
