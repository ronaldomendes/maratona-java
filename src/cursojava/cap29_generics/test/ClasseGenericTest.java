package cursojava.cap29_generics.test;

import cursojava.cap29_generics.classes.Carro;
import cursojava.cap29_generics.classes.Computador;

import java.util.ArrayList;
import java.util.List;

public class ClasseGenericTest {
    public static void main(String[] args) {
        List<Carro> carrosDisponiveis = new ArrayList<>();
        carrosDisponiveis.add(new Carro("Fusca 1500"));
        carrosDisponiveis.add(new Carro("Fiat 147"));

        ObjetosAlugaveis<Carro> carroAlugavel = new ObjetosAlugaveis<>(carrosDisponiveis);
        Carro carro = carroAlugavel.getObjetoDisponivel();
        System.out.println("Usando o carro por um mês");
        carroAlugavel.devolverObjeto(carro);

        System.out.println("\n============================\n");

        List<Computador> computadoresDisponiveis = new ArrayList<>();
        computadoresDisponiveis.add(new Computador("Pentium 3 600 Mhz"));
        computadoresDisponiveis.add(new Computador("Pentium 4"));

        ObjetosAlugaveis<Computador> computadorAlugavel = new ObjetosAlugaveis<>(computadoresDisponiveis);
        Computador computadorAlugado = computadorAlugavel.getObjetoDisponivel();
        System.out.println("Usando o computador por um mês");
        computadorAlugavel.devolverObjeto(computadorAlugado);
    }
}

class ObjetosAlugaveis<T> {
    private List<T> objetosDisponiveis = new ArrayList<>();

    public ObjetosAlugaveis(List<T> objetosDisponiveis) {
        this.objetosDisponiveis = objetosDisponiveis;
    }

    //    Alugar
    public T getObjetoDisponivel() {
        T obj = objetosDisponiveis.remove(0);
        System.out.println("Alugando objeto: " + obj);
        System.out.println("Objetos disponíveis: " + objetosDisponiveis);
        return obj;
    }

    public void devolverObjeto(T obj) {
        System.out.println("Devolvendo objeto: " + obj);
        objetosDisponiveis.add(obj);
        System.out.println("Objetos disponíveis: " + objetosDisponiveis);
    }
}

class DoisAtributos<T, X> {
    T um;
    X dois;

    public DoisAtributos(T um, X dois) {
        this.um = um;
        this.dois = dois;
    }

    public T getUm() {
        return um;
    }

    public void setUm(T um) {
        this.um = um;
    }

    public X getDois() {
        return dois;
    }

    public void setDois(X dois) {
        this.dois = dois;
    }
}
