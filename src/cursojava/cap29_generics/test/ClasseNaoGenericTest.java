package cursojava.cap29_generics.test;

import cursojava.cap29_generics.classes.Carro;
import cursojava.cap29_generics.classes.Computador;

import java.util.ArrayList;
import java.util.List;

public class ClasseNaoGenericTest {
    public static void main(String[] args) {
        CarroAlugavel carroAlugavel = new CarroAlugavel();
        Carro carroAlugado = carroAlugavel.getCarroDisponivel();
        System.out.println("Usando o carro por um mês");
        carroAlugavel.devolverCarro(carroAlugado);

        System.out.println("\n============================\n");

        ComputadorAlugavel computadorAlugavel = new ComputadorAlugavel();
        Computador computadorAlugado = computadorAlugavel.getComputadorDisponivel();
        System.out.println("Usando o computador por um mês");
        computadorAlugavel.devolverComputador(computadorAlugado);
    }
}

class CarroAlugavel {
    private List<Carro> carrosDisponiveis = new ArrayList<>();

    {
        carrosDisponiveis.add(new Carro("Fusca 1500"));
        carrosDisponiveis.add(new Carro("Fiat 147"));
    }

//    Alugar

    public Carro getCarroDisponivel() {
        Carro carro = carrosDisponiveis.remove(0);
        System.out.println("Alugando carro: " + carro);
        System.out.println("Carros disponíveis: " + carrosDisponiveis);
        return carro;
    }

    public void devolverCarro(Carro carro) {
        System.out.println("Devolvendo carro: " + carro);
        carrosDisponiveis.add(carro);
        System.out.println("Carros disponíveis: " + carrosDisponiveis);
    }
}

class ComputadorAlugavel {
    private List<Computador> computadoresDisponiveis = new ArrayList<>();

    {
        computadoresDisponiveis.add(new Computador("Pentium 3 600 Mhz"));
        computadoresDisponiveis.add(new Computador("Pentium 4"));
    }

//    Alugar

    public Computador getComputadorDisponivel() {
        Computador pc = computadoresDisponiveis.remove(0);
        System.out.println("Alugando computador: " + pc);
        System.out.println("Computadores disponíveis: " + computadoresDisponiveis);
        return pc;
    }

    public void devolverComputador(Computador pc) {
        System.out.println("Devolvendo computador: " + pc);
        computadoresDisponiveis.add(pc);
        System.out.println("Computadores disponíveis: " + computadoresDisponiveis);
    }
}

