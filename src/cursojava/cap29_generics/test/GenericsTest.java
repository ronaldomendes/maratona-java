package cursojava.cap29_generics.test;

import cursojava.cap28_collections.classes.Consumidor;

import java.util.ArrayList;
import java.util.List;

public class GenericsTest {
    public static void main(String[] args) {
        List lista = new ArrayList();
        lista.add(1L);
        lista.add("formato");
        lista.add("antigo");
        lista.add("de");
        lista.add("lista");
        lista.add(new Consumidor("Godofredo", "321"));
        System.out.println(lista);

        add(lista, 1L);

        for (Object obj : lista) {
            if (obj instanceof String) System.out.println(obj);
            if (obj instanceof Long) System.out.println(obj);
            if (obj instanceof Consumidor) System.out.println(obj);
        }

        List<String> stringList = new ArrayList<>();
        stringList.add("Novo");
        stringList.add("formato");
        stringList.add("de");
        stringList.add("lista");

        for (String value : stringList) {
            System.out.println(value);
        }
    }

    public static void add(List list, Long l) {
        list.add(l);
    }
}
