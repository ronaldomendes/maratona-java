package cursojava.cap29_generics.test;

import java.util.ArrayList;
import java.util.List;

public class MetodoGenericTest {
    public static void main(String[] args) {
        criarArray(new Cachorro());
//        List<Cachorro> cachorroList = criarArrayList(new Cachorro());
//        System.out.println(cachorroList);
    }

    public static <T extends Animal> void criarArray(T t) {
        List<T> lista = new ArrayList<>();
        lista.add(t);
        System.out.println(lista);
    }

    public static <T> List<T> criarArrayList(T t) {
        List<T> lista = new ArrayList<>();
        lista.add(t);
        return lista;
    }


}
