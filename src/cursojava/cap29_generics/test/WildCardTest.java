package cursojava.cap29_generics.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

abstract class Animal {
    public abstract void som();
}

class Cachorro extends Animal implements Comparable {

    @Override
    public void som() {
        System.out.println("Au au...");
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}

class Gato extends Animal {

    @Override
    public void som() {
        System.out.println("Miauuu...");
    }
}

public class WildCardTest {
    public static void main(String[] args) {
        Cachorro[] cachorros = {new Cachorro(), new Cachorro()};
        Gato[] gatos = {new Gato(), new Gato()};
//        consultarAnimais(cachorros);
//        consultarAnimais(gatos);

        List<Cachorro> cachorroList = new ArrayList<>();
        cachorroList.add(new Cachorro());

        List<Gato> gatoList = new ArrayList<>();
        gatoList.add(new Gato());

        consultarAnimaisList(cachorroList);
        consultarAnimaisList(gatoList);
        ordenarLista(cachorroList);
    }

    public static void consultarAnimais(Animal[] animals) {
        for (Animal animal : animals) {
            animal.som();
        }
//        animals[1] = new Gato();
    }

    public static void consultarAnimaisList(List<? extends Animal> animals) {
        for (Animal animal : animals) {
            animal.som();
        }
    }

    public static void consultarCachorroList(List<? super Cachorro> cachorros) {
        Cachorro cachorro = new Cachorro();
        Animal animal = new Cachorro();
        Object object = new Cachorro();
        cachorros.add(new Cachorro());
    }

    public static void ordenarLista(List<? extends Comparable> list) {
        Collections.sort(list);
    }
}
