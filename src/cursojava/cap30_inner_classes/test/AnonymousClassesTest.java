package cursojava.cap30_inner_classes.test;

import cursojava.cap29_generics.classes.Carro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Animal {
    public void andar() {
        System.out.println("Andando...");
    }
}

public class AnonymousClassesTest {
    public static void main(String[] args) {
        Animal animal = new Animal() {
            @Override
            public void andar() {
                System.out.println("Andando anonimamente...");
            }
        };
        animal.andar();

        List<Carro> carros = new ArrayList<>();
        carros.add(new Carro("Gurgel"));
        carros.add(new Carro("Brasilia"));
        carros.add(new Carro("Kombi"));

//        Modo 01
//        Collections.sort(carros, new CarroComparator());

//        Modo 02
        Collections.sort(carros, new Comparator<Carro>() {
            @Override
            public int compare(Carro o1, Carro o2) {
                return o1.getNome().compareTo(o2.getNome());
            }
        });

//        Modo 03 - Lambda
//        carros.sort((o1, o2) -> o1.getNome().compareTo(o2.getNome()));

//        Modo 04 - Comparator
//        carros.sort(Comparator.comparing(Carro::getNome));

//        Modo 05
//        carros.sort(new CarroComparator());
        System.out.println(carros);
        carros.forEach(System.out::println);
    }
}

class CarroComparator implements Comparator<Carro> {

    @Override
    public int compare(Carro o1, Carro o2) {
        return o1.getNome().compareTo(o2.getNome());
    }
}
