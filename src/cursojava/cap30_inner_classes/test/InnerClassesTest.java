package cursojava.cap30_inner_classes.test;

public class InnerClassesTest {
    public static void main(String[] args) {
        Externa externa = new Externa();
        Externa.Interna interna01 = externa.new Interna();
        interna01.verClasseInterna();

        Externa.Interna interna02 = new Externa().new Interna();
        interna02.verClasseInterna();
    }
}

class Externa {
    private String nome = "Obi Wan Kenobi";

    class Interna {
        public void verClasseInterna() {
            System.out.println(nome);
            System.out.println(this);
            System.out.println(Externa.this);
        }
    }
}

