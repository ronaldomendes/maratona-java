package cursojava.cap30_inner_classes.test;

class ClasseExterna {
    static class ClasseInterna {
        public void imprimir() {
            System.out.println("Imprimindo...");
        }
    }
}

public class InternalInnerClassesTest {
    public static void main(String[] args) {
        ClasseExterna.ClasseInterna interna = new ClasseExterna.ClasseInterna();
        interna.imprimir();
        InternaMain internaMain = new InternaMain();
        internaMain.imprimir();
    }

    static class InternaMain {
        public void imprimir() {
            System.out.println("Imprimindo em um método dentro da main");
        }
    }
}
