package cursojava.cap30_inner_classes.test;

public class LocalClassesTest {
    private String nome = "Godofredo";

    public void novoMetodo(String test) {
        String sobrenome = "Silva";
        class Interna {
            public void imprimeNomeExterno() {
                System.out.println(nome);
                System.out.println(sobrenome);
                System.out.println(test);
            }
        }

//        Só é possível chamar a classe interna em outra classe
//        após a criação de uma instância dentro desta classe
        Interna interna = new Interna();
        interna.imprimeNomeExterno();
    }

    public static void main(String[] args) {
        LocalClassesTest externa = new LocalClassesTest();
        externa.novoMetodo("Olá!");
    }
}
