package cursojava.cap31_jdbc.classes;

import java.util.Objects;

public class Carro {
    private Integer id;
    private String placa;
    private String nome;
    private Comprador comprador;

    public Carro() {
    }

    public Carro(Integer id, String placa, String nome, Comprador comprador) {
        this.id = id;
        this.placa = placa;
        this.nome = nome;
        this.comprador = comprador;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carro carro = (Carro) o;
        return Objects.equals(id, carro.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Carro{");
        sb.append("id=").append(id);
        sb.append(", placa='").append(placa).append('\'');
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", comprador=").append(comprador);
        sb.append('}');
        return sb.toString();
    }
}
