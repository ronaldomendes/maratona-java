package cursojava.cap31_jdbc.db;

import cursojava.cap31_jdbc.classes.Carro;
import cursojava.cap31_jdbc.classes.Comprador;
import cursojava.cap31_jdbc.conn.ConexaoFactory;
import cursojava.cap31_jdbc.interfaces.CarroDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarroDAOImplNoSQL implements CarroDAO {

    @Override
    public void save(Carro carro) {
        String sql = "INSERT INTO carro VALUES(default, ?, ?, ?)";

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, carro.getPlaca());
            statement.setString(2, carro.getNome());
            statement.setInt(3, carro.getComprador().getId());
            statement.executeUpdate();
            System.out.println("Registro cadastrado com sucesso\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Carro carro) {
        if (carro == null || carro.getId() == null) {
            System.out.println("Não foi possível atualizar o registro\n");
            return;
        }

        String sql = "UPDATE carro SET placa=?, nome=? WHERE id=?";

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, carro.getPlaca());
            statement.setString(2, carro.getNome());
            statement.setInt(3, carro.getId());
            statement.executeUpdate();
            System.out.println("Registro atualizado com sucesso\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Carro carro) {
        if (carro == null || carro.getId() == null) {
            System.out.println("Não foi possível excluir o registro\n");
            return;
        }

        String sql = "DELETE FROM carro WHERE id=?";
        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, carro.getId());
            statement.executeUpdate();
            System.out.println("Registro excluído com sucesso\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Carro> selectAll() {
        String sql = "SELECT id, placa, nome, comprador_id FROM carro";
        List<Carro> carroList = new ArrayList<>();

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql);
             ResultSet set = statement.executeQuery()) {
            while (set.next()) {
                Comprador comprador = CompradorDAO.selectById(set.getInt(4));
                carroList.add(new Carro(set.getInt(1), set.getString(2),
                        set.getString(3), comprador));
            }
            return carroList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Carro> selectByName(String nome) {
        String sql = "SELECT id, placa, nome, comprador_id FROM carro WHERE nome LIKE ?";
        List<Carro> carroList = new ArrayList<>();

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, "%" + nome + "%");
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                Comprador comprador = CompradorDAO.selectById(set.getInt(4));
                carroList.add(new Carro(set.getInt("id"), set.getString("placa"),
                        set.getString("nome"), comprador));
            }
            ConexaoFactory.close(conn, statement, set);
            return carroList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
