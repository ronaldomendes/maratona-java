package cursojava.cap31_jdbc.db;

import cursojava.cap31_jdbc.classes.Comprador;
import cursojava.cap31_jdbc.conn.ConexaoFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompradorDAO {
    public static void save(Comprador comprador) {
        String sql = "INSERT INTO comprador VALUES(default, ?, ?)";

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, comprador.getCpf());
            statement.setString(2, comprador.getNome());
            statement.executeUpdate();
            System.out.println("Registro cadastrado com sucesso\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void update(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível atualizar o registro\n");
            return;
        }

        String sql = "UPDATE comprador SET cpf=?, nome=? WHERE id=?";

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, comprador.getCpf());
            statement.setString(2, comprador.getNome());
            statement.setInt(3, comprador.getId());
            statement.executeUpdate();
            System.out.println("Registro atualizado com sucesso\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível excluir o registro\n");
            return;
        }

        String sql = "DELETE FROM comprador WHERE id=?";
        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, comprador.getId());
            statement.executeUpdate();
            System.out.println("Registro excluído com sucesso\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Comprador> selectAll() {
        String sql = "SELECT id, cpf, nome FROM comprador";
        List<Comprador> compradorList = new ArrayList<>();

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql);
             ResultSet set = statement.executeQuery()) {
            while (set.next()) {
                compradorList.add(new Comprador(set.getInt(1),
                        set.getString(2), set.getString(3)));
            }
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Comprador> selectByName(String nome) {
        String sql = "SELECT id, cpf, nome FROM comprador WHERE nome LIKE ?";
        List<Comprador> compradorList = new ArrayList<>();

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, "%" + nome + "%");
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                compradorList.add(new Comprador(set.getInt("id"),
                        set.getString("cpf"), set.getString("nome")));
            }
            ConexaoFactory.close(conn, statement, set);
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Comprador selectById(Integer id) {
        String sql = "SELECT id, cpf, nome FROM comprador WHERE id=?";
        Comprador comprador = null;

        try (Connection conn = ConexaoFactory.getConexao();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                comprador = new Comprador(set.getInt("id"),
                        set.getString("cpf"), set.getString("nome"));
            }
            ConexaoFactory.close(conn, statement, set);
            return comprador;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
