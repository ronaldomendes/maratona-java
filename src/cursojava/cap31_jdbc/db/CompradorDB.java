package cursojava.cap31_jdbc.db;

import cursojava.cap31_jdbc.classes.Comprador;
import cursojava.cap31_jdbc.classes.MyRowSetListener;
import cursojava.cap31_jdbc.conn.ConexaoFactory;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.JdbcRowSet;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CompradorDB {
    public static void save(Comprador comprador) {
        String cpf = comprador.getCpf();
        String nome = comprador.getNome();

        String sql = String.format("INSERT INTO comprador VALUES(default, '%s', '%s')", cpf, nome);
        Connection conn = ConexaoFactory.getConexao();

        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
            ConexaoFactory.close(conn, statement);
            System.out.println("Registro cadastrado com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void saveTransaction() throws SQLException {
        String sql1 = "INSERT INTO comprador VALUES(default, 'xxx-xxx-xxx-01', 'nome01')";
        String sql2 = "INSERT INTO comprador VALUES(default, 'xxx-xxx-xxx-02', 'nome02')";
        String sql3 = "INSERT INTO comprador VALUES(default, 'xxx-xxx-xxx-03', 'nome03')";
        Connection conn = ConexaoFactory.getConexao();
        Savepoint savepoint = null;

        try {
            conn.setAutoCommit(false);
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql1);
            savepoint = conn.setSavepoint("One");

//            em caso de drivers que não suportam multiplas conexões,
//            é possível reaproveitar a conexão com o comando abaixo
//            conn.releaseSavepoint(savepoint);

            statement.executeUpdate(sql2);
            if (true) throw new SQLException();
            statement.executeUpdate(sql3);
            conn.commit();

            ConexaoFactory.close(conn, statement);
            System.out.println("Registro cadastrado com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
            conn.rollback(savepoint);
            conn.commit();
        }
    }

    public static void update(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível atualizar o registro");
            return;
        }

        Integer id = comprador.getId();
        String cpf = comprador.getCpf();
        String nome = comprador.getNome();

        String sql = String.format("UPDATE comprador SET cpf='%s', nome='%s' WHERE id='%d'", cpf, nome, id);
        Connection conn = ConexaoFactory.getConexao();

        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
            ConexaoFactory.close(conn, statement);
            System.out.println("Registro atualizado com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updatePreparedStatement(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível atualizar o registro");
            return;
        }

        String sql = "UPDATE comprador SET cpf=?, nome=? WHERE id=?";
        Connection conn = ConexaoFactory.getConexao();

        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, comprador.getCpf());
            statement.setString(2, comprador.getNome());
            statement.setInt(3, comprador.getId());
            statement.executeUpdate();
            ConexaoFactory.close(conn, statement);
            System.out.println("Registro atualizado com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateRowSet(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível atualizar o registro");
            return;
        }

        String sql = "SELECT * FROM comprador WHERE id=?";
        JdbcRowSet conn = ConexaoFactory.getRowSetConnection();
        conn.addRowSetListener(new MyRowSetListener());

        try {
            conn.setCommand(sql);
            conn.setInt(1, comprador.getId());
            conn.execute();

            conn.next();
            conn.updateString("nome", comprador.getNome());
            conn.updateString("cpf", comprador.getCpf());
            conn.updateRow();

            ConexaoFactory.close(conn);
            System.out.println("Registro atualizado com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateCachedRowSet(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível atualizar o registro");
            return;
        }

        String sql = "SELECT * FROM comprador WHERE id=?";
        CachedRowSet conn = ConexaoFactory.getCachedRowSetConnection();

        try {
            conn.setCommand(sql);
            conn.setInt(1, comprador.getId());
            conn.execute();

            conn.next();
            conn.updateString("nome", comprador.getNome());
            conn.updateString("cpf", comprador.getCpf());
            conn.updateRow();

//            testando para causar um erro enquanto o update sincroniza com o banco de dados
//            (enquanto o método tenta sincronizar, faço uma atualização manualmente para causar o erro)
            Thread.sleep(10000);

            conn.acceptChanges();
            System.out.println("Registro atualizado com sucesso");
        } catch (SQLException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Comprador comprador) {
        if (comprador == null || comprador.getId() == null) {
            System.out.println("Não foi possível excluir o registro");
            return;
        }

        Integer id = comprador.getId();
        String sql = String.format("DELETE FROM comprador WHERE id='%d'", id);
        Connection conn = ConexaoFactory.getConexao();

        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
            ConexaoFactory.close(conn, statement);
            System.out.println("Registro excluído com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Comprador> selectAll() {
        String sql = "SELECT id, cpf, nome FROM comprador";
        Connection conn = ConexaoFactory.getConexao();
        List<Comprador> compradorList = new ArrayList<>();

        try {
            Statement statement = conn.createStatement();
            ResultSet set = statement.executeQuery(sql);
            while (set.next()) {
                compradorList.add(new Comprador(set.getInt(1),
                        set.getString(2), set.getString(3)));
            }
            ConexaoFactory.close(conn, statement, set);
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Comprador> selectByName(String nome) {
        String sql = "SELECT id, cpf, nome FROM comprador WHERE nome LIKE '%" + nome + "%'";
        Connection conn = ConexaoFactory.getConexao();
        List<Comprador> compradorList = new ArrayList<>();

        try {
            Statement statement = conn.createStatement();
            ResultSet set = statement.executeQuery(sql);
            while (set.next()) {
                compradorList.add(new Comprador(set.getInt("id"),
                        set.getString("cpf"), set.getString("nome")));
            }
            ConexaoFactory.close(conn, statement, set);
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Comprador> selectByNamePreparedStatement(String nome) {
        String sql = "SELECT id, cpf, nome FROM comprador WHERE nome LIKE ?";
        Connection conn = ConexaoFactory.getConexao();
        List<Comprador> compradorList = new ArrayList<>();

        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + nome + "%");
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                compradorList.add(new Comprador(set.getInt("id"),
                        set.getString("cpf"), set.getString("nome")));
            }
            ConexaoFactory.close(conn, statement, set);
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Comprador> selectByNameRowSet(String nome) {
        String sql = "SELECT id, cpf, nome FROM comprador WHERE nome LIKE ?";
        JdbcRowSet conn = ConexaoFactory.getRowSetConnection();
        conn.addRowSetListener(new MyRowSetListener());
        List<Comprador> compradorList = new ArrayList<>();

        try {
            conn.setCommand(sql);
            conn.setString(1, "%" + nome + "%");
            conn.execute();
            while (conn.next()) {
                compradorList.add(new Comprador(conn.getInt("id"),
                        conn.getString("cpf"), conn.getString("nome")));
            }
            ConexaoFactory.close(conn);
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Comprador> selectByNameCallableStatement(String nome) {
        String sql = "call SP_GetCompradoresByNome(?)";
        Connection conn = ConexaoFactory.getConexao();
        List<Comprador> compradorList = new ArrayList<>();

        try {
            CallableStatement statement = conn.prepareCall(sql);
            statement.setString(1, "%" + nome + "%");
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                compradorList.add(new Comprador(set.getInt("id"),
                        set.getString("cpf"), set.getString("nome")));
            }
            ConexaoFactory.close(conn, statement, set);
            return compradorList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void selectMetaData() {
        String sql = "SELECT * FROM comprador";
        Connection conn = ConexaoFactory.getConexao();

        try {
            Statement statement = conn.createStatement();
            ResultSet set = statement.executeQuery(sql);
            ResultSetMetaData data = set.getMetaData();

            set.next();
            int qtdRows = data.getColumnCount();
            System.out.println("Quantidade colunas: " + qtdRows);

            for (int i = 1; i <= qtdRows; i++) {
                System.out.print("Tabela: " + data.getTableName(i));
                System.out.print(" - Nome coluna: " + data.getColumnName(i));
                System.out.println(" - Tamanho coluna: " + data.getColumnDisplaySize(i));
            }
            ConexaoFactory.close(conn, statement, set);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void checkDriverStatus() {
        Connection conn = ConexaoFactory.getConexao();
        try {
            DatabaseMetaData metaData = conn.getMetaData();

            if (metaData.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY)) {
                System.out.print("Suporta TYPE_FORWARD_ONLY");
                if (metaData.supportsResultSetConcurrency(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE)) {
                    System.out.println(" e também suporta CONCUR_UPDATABLE");
                }
            }

            if (metaData.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE)) {
                System.out.print("Suporta TYPE_SCROLL_INSENSITIVE");
                if (metaData.supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
                    System.out.println(" e também suporta CONCUR_UPDATABLE");
                }
            }

            if (metaData.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE)) {
                System.out.print("Suporta TYPE_SCROLL_SENSITIVE");
                if (metaData.supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
                    System.out.println(" e também suporta CONCUR_UPDATABLE");
                }
            }
            ConexaoFactory.close(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void testTypeScroll() {
        String sql = "SELECT id, cpf, nome FROM comprador";
        Connection conn = ConexaoFactory.getConexao();

        try {
            Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet set = statement.executeQuery(sql);
            if (set.last()) {
                System.out.println("Última linha: " + new Comprador(set.getInt(1),
                        set.getString(2), set.getString(3)));
                System.out.println("Número última linha: " + set.getRow());
            }

            System.out.println("Retornando para a primeira linha?" + set.first());
            System.out.println("Número da linha atual: " + set.getRow());

            set.absolute(4);
            System.out.println("Mostrando a linha 4: " + new Comprador(set.getInt(1),
                    set.getString(2), set.getString(3)));

            set.relative(-1);
            System.out.println("Mostrando a linha 3: " + new Comprador(set.getInt(1),
                    set.getString(2), set.getString(3)));

            System.out.println(set.isLast());
            System.out.println(set.isFirst());

            set.afterLast();
            System.out.println("\n=================================");
            while (set.previous()) {
                System.out.println(new Comprador(set.getInt(1),
                        set.getString(2), set.getString(3)));
            }
            ConexaoFactory.close(conn, statement, set);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNomesToLowerCase() {
        String sql = "SELECT id, cpf, nome FROM comprador";
        Connection conn = ConexaoFactory.getConexao();

        try {
            DatabaseMetaData metaData = conn.getMetaData();

            Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet set = statement.executeQuery(sql);

            System.out.println(metaData.updatesAreDetected(ResultSet.TYPE_SCROLL_INSENSITIVE));
            System.out.println(metaData.insertsAreDetected(ResultSet.TYPE_SCROLL_INSENSITIVE));
            System.out.println(metaData.deletesAreDetected(ResultSet.TYPE_SCROLL_INSENSITIVE));

            if (set.next()) {
                set.updateString("nome", set.getString("nome").toUpperCase());
                set.updateRow();
//                if (set.rowUpdated()) {
//                    System.out.println("Linha atualizada");
//                }
            }

//            set.beforeFirst();
//            while (set.next()) {
//                System.out.println(set.getString("nome"));
//            }

//            update/insert
//            set.absolute(2);
//            String nome = set.getString("nome");
//            set.moveToInsertRow();
//            set.updateString("nome", nome.toUpperCase());
//            set.updateString("cpf", "345.123.235-23");
//            set.insertRow();
//            set.moveToCurrentRow();
//            System.out.println(set.getString("nome") + " coluna " + set.getRow());

//            delete
            set.absolute(7);
            set.deleteRow();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
