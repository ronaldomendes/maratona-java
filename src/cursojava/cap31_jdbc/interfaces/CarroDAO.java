package cursojava.cap31_jdbc.interfaces;

import cursojava.cap31_jdbc.classes.Carro;

import java.util.List;

public interface CarroDAO {
    void save(Carro carro);

    void update(Carro carro);

    void delete(Carro carro);

    List<Carro> selectAll();

    List<Carro> selectByName(String nome);
}
