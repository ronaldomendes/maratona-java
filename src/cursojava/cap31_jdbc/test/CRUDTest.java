package cursojava.cap31_jdbc.test;

import java.util.Scanner;

public class CRUDTest {
    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        while (true) {
            menu();
            op = Integer.parseInt(teclado.nextLine());
            if (op == 0) {
                System.out.println("Saindo do sistema...");
                break;
            }

            if (op == 1) {
                CompradorCRUD.menu();
                op = Integer.parseInt(teclado.nextLine());
                CompradorCRUD.execute(op);
            }

            if (op == 2) {
                CarroCRUD.menu();
                op = Integer.parseInt(teclado.nextLine());
                CarroCRUD.execute(op);
            }
        }
    }

    public static void menu() {
        System.out.println("Selecione uma opção");
        System.out.println("1. Comprador");
        System.out.println("2. Carro");
        System.out.println("0. Sair");
    }
}
