package cursojava.cap31_jdbc.test;

import cursojava.cap31_jdbc.classes.Carro;
import cursojava.cap31_jdbc.classes.Comprador;
import cursojava.cap31_jdbc.db.CarroDAOImpl;
import cursojava.cap31_jdbc.interfaces.CarroDAO;

import java.util.List;
import java.util.Scanner;

public class CarroCRUD {
    public static Scanner teclado = new Scanner(System.in);
    private static CarroDAO dao = new CarroDAOImpl();

    public static void execute(int op) {
        switch (op) {
            case 1:
                create();
                break;
            case 2:
                update();
                break;
            case 3:
                readAll();
                break;
            case 4:
                System.out.println("Digite o nome: ");
                readAllByNome(teclado.nextLine());
                break;
            case 5:
                delete();
                break;
        }
    }

    private static void create() {
        Carro carro = new Carro();
        System.out.println("Nome: ");
        carro.setNome(teclado.nextLine());
        System.out.println("Placa: ");
        carro.setPlaca(teclado.nextLine());
        System.out.println("Selecione um dos compradores abaixo");
        List<Comprador> list = CompradorCRUD.readAll();
        carro.setComprador(list.get(Integer.parseInt(teclado.nextLine())));
        dao.save(carro);
    }

    private static List<Carro> readAll() {
        List<Carro> list = dao.selectAll();

        if (list == null) return null;
        for (int i = 0; i < list.size(); i++) {
            Carro c = list.get(i);
            System.out.println(String.format("%d) %s - %s (%s)",
                    i, c.getPlaca(), c.getNome(), c.getComprador().getNome()));
        }
        return list;
    }

    private static void readAllByNome(String nome) {
        List<Carro> list = dao.selectByName(nome);

        if (list == null) return;
        for (int i = 0; i < list.size(); i++) {
            Carro c = list.get(i);
            System.out.println(String.format("%d) %s - %s (%s)",
                    i, c.getPlaca(), c.getNome(), c.getComprador().getNome()));
        }
    }

    private static void update() {
        System.out.println("Selecione um dos carros abaixo");
        List<Carro> list = readAll();
        Carro c = list.get(Integer.parseInt(teclado.nextLine()));
        System.out.println("Digite o nome ou tecle ENTER para continuar");
        String nome = teclado.nextLine();
        System.out.println("Digite a placa ou tecle ENTER para continuar");
        String placa = teclado.nextLine();

        if (!nome.isEmpty()) c.setNome(nome);
        if (!placa.isEmpty()) c.setPlaca(placa);

        dao.update(c);
    }

    public static void delete() {
        System.out.println("Selecione um dos carros abaixo");
        List<Carro> list = readAll();
        int index = Integer.parseInt(teclado.nextLine());
        System.out.println("Tem certeza? S/N");
        String op = teclado.nextLine();
        if (op.startsWith("s")) {
            dao.delete(list.get(index));
        }
    }

    public static void menu() {
        System.out.println("Digite uma opção para começar");
        System.out.println("1. Inserir carro");
        System.out.println("2. Atualizar carro");
        System.out.println("3. Listar todos os carros");
        System.out.println("4. Buscar carro por nome");
        System.out.println("5. Deletar carro");
        System.out.println("9. Voltar");
    }
}
