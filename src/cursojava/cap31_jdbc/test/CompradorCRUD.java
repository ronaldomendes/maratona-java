package cursojava.cap31_jdbc.test;

import cursojava.cap31_jdbc.classes.Comprador;
import cursojava.cap31_jdbc.db.CompradorDAO;

import java.util.List;
import java.util.Scanner;

public class CompradorCRUD {
    public static Scanner teclado = new Scanner(System.in);

    public static void execute(int op) {
        switch (op) {
            case 1:
                create();
                break;
            case 2:
                update();
                break;
            case 3:
                readAll();
                break;
            case 4:
                System.out.println("Digite o nome: ");
                readAllByNome(teclado.nextLine());
                break;
            case 5:
                delete();
                break;
        }
    }

    public static void create() {
        Comprador comprador = new Comprador();
        System.out.println("Nome: ");
        comprador.setNome(teclado.nextLine());
        System.out.println("CPF: ");
        comprador.setNome(teclado.nextLine());
        CompradorDAO.save(comprador);
    }

    public static List<Comprador> readAll() {
        List<Comprador> list = CompradorDAO.selectAll();

        if (list == null) return null;
        for (int i = 0; i < list.size(); i++) {
            Comprador c = list.get(i);
            System.out.println(String.format("%d) %s - %s", i, c.getCpf(), c.getNome()));
        }
        return list;
    }

    public static void readAllByNome(String nome) {
        List<Comprador> list = CompradorDAO.selectByName(nome);

        if (list == null) return;
        for (int i = 0; i < list.size(); i++) {
            Comprador c = list.get(i);
            System.out.println(String.format("%d) %s - %s", i, c.getCpf(), c.getNome()));
        }
    }

    public static void update() {
        System.out.println("Selecione um dos compradores abaixo");
        List<Comprador> list = readAll();
        Comprador c = list.get(Integer.parseInt(teclado.nextLine()));
        System.out.println("Digite o nome ou tecle ENTER para continuar");
        String nome = teclado.nextLine();
        System.out.println("Digite o CPF ou tecle ENTER para continuar");
        String cpf = teclado.nextLine();

        if (!nome.isEmpty()) c.setNome(nome);
        if (!cpf.isEmpty()) c.setCpf(cpf);

        CompradorDAO.update(c);
    }

    public static void delete() {
        System.out.println("Selecione um dos compradores abaixo");
        List<Comprador> list = readAll();
        int index = Integer.parseInt(teclado.nextLine());
        System.out.println("Tem certeza? S/N");
        String op = teclado.nextLine();
        if (op.startsWith("s")) {
            CompradorDAO.delete(list.get(index));
        }
    }

    public static void menu() {
        System.out.println("Digite uma opção para começar");
        System.out.println("1. Inserir comprador");
        System.out.println("2. Atualizar comprador");
        System.out.println("3. Listar todos os compradores");
        System.out.println("4. Buscar comprador por nome");
        System.out.println("5. Deletar comprador");
        System.out.println("9. Voltar");
    }
}
