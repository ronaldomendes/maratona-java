package cursojava.cap31_jdbc.test;

import cursojava.cap31_jdbc.classes.Comprador;
import cursojava.cap31_jdbc.db.CompradorDB;

import java.sql.SQLException;
import java.util.List;

public class ConexaoTest {
    public static void main(String[] args) {
//        insert();
//        update();
//        delete();
//        List<Comprador> listAll = selectAll();
//        listAll.forEach(System.out::println);
//        List<Comprador> listByName = selectByName("za");
//        listByName.forEach(System.out::println);
//        CompradorDB.selectMetaData();
//        CompradorDB.checkDriverStatus();
//        CompradorDB.testTypeScroll();
//        CompradorDB.updateNomesToLowerCase();
//        System.out.println(CompradorDB.selectByNamePreparedStatement("pi' or 'X'='X"));
//        System.out.println(CompradorDB.selectByNamePreparedStatement("pi"));
//        CompradorDB.updatePreparedStatement(new Comprador(2, "xxx.xxx.xxx-xx", "Andy Hahaha..."));
//        System.out.println(CompradorDB.selectByNameCallableStatement("%pi%"));
//        System.out.println(CompradorDB.selectByNameRowSet("pi"));
//        CompradorDB.updateRowSet(new Comprador(2, "650.654.320-80", "Andy Panda"));
//        CompradorDB.updateCachedRowSet(new Comprador(2, "650.654.320-80", "andy panda"));
        try {
            CompradorDB.saveTransaction();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insert() {
        Comprador comprador = new Comprador("650.654.320-80", "Tonho");
        CompradorDB.save(comprador);
    }

    public static void update() {
        Comprador comprador = new Comprador(2, "650.654.320-80", "Andy Panda");
        CompradorDB.update(comprador);
    }

    public static void delete() {
        Comprador comprador = new Comprador();
        comprador.setId(1);
        CompradorDB.delete(comprador);
    }

    public static List<Comprador> selectAll() {
        return CompradorDB.selectAll();
    }

    public static List<Comprador> selectByName(String nome) {
        return CompradorDB.selectByName(nome);
    }
}
