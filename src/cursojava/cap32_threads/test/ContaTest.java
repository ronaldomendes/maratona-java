package cursojava.cap32_threads.test;

import cursojava.cap32_threads.classes.Conta;

public class ContaTest implements Runnable {
    private final Conta conta = new Conta();

    public static void main(String[] args) {
        ContaTest test = new ContaTest();
        Thread godofredo = new Thread(test, "Godofredo");
        Thread creuza = new Thread(test, "Creuza");
        imprime();
        godofredo.start();
        creuza.start();
    }

    public static void imprime() {
        synchronized (ContaTest.class) {
            System.out.println("Imprimindo em um método synchronized");
        }
    }

//    criando uma "fila" para que o método seja acessado de forma ordenada dizendo que o método é "synchronized"
//    Modo 01 - método sincronizado
    private synchronized void saqueSynchronized(int valor) {
        if (conta.getSaldo() >= valor) {
            System.out.println(Thread.currentThread().getName() + " está sacando...");

//            mesmo adicionando o sleep, uma nova thread não pode executar antes que a anterior termine
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            conta.saque(valor);
            System.out.println(Thread.currentThread().getName() + " completou o saque... Saldo: " + conta.getSaldo());
        } else {
            System.out.println("Sem dinheiro para " + Thread.currentThread().getName() +
                    " efetuar o saque " + conta.getSaldo());
        }
    }

//    Modo 02 - bloco sincronizado
    private void saque(int valor) {
        synchronized (conta) {
            if (conta.getSaldo() >= valor) {
                System.out.println(Thread.currentThread().getName() + " está sacando...");

//            mesmo adicionando o sleep, uma nova thread não pode executar antes que a anterior termine
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                conta.saque(valor);
                System.out.println(Thread.currentThread().getName() + " completou o saque... Saldo: " + conta.getSaldo());
            } else {
                System.out.println("Sem dinheiro para " + Thread.currentThread().getName() +
                        " efetuar o saque " + conta.getSaldo());
            }
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            saque(10);
            if (conta.getSaldo() < 0) {
                System.out.println("Ficou liso de vez!");
            }
        }
    }
}
