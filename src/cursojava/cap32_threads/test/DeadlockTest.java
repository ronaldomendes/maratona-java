package cursojava.cap32_threads.test;

public class DeadlockTest {
    private static Object lock01 = new Object();
    private static Object lock02 = new Object();

    public static void main(String[] args) {
        new ThreadExemplo01().start();
        new ThreadExemplo02().start();
    }

    private static class ThreadExemplo01 extends Thread {
        public void run() {
            synchronized (lock01) {
                System.out.println("Thread 01: Segurando lock 01");
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Thread 01: Esperando pelo lock 02");
                synchronized (lock02) {
                    System.out.println("Thread 01: Segurando lock 01 e lock 02");
                }
            }
        }
    }

    private static class ThreadExemplo02 extends Thread {
        public void run() {
            synchronized (lock01) {
                System.out.println("Thread 02: Segurando lock 02");
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Thread 02: Esperando pelo lock 01");
                synchronized (lock02) {
                    System.out.println("Thread 02: Segurando lock 02 e lock 01");
                }
            }
        }
    }
}
