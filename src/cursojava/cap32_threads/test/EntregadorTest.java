package cursojava.cap32_threads.test;

import cursojava.cap32_threads.classes.Entregador;
import cursojava.cap32_threads.classes.ListaMembros;

import javax.swing.*;

public class EntregadorTest {
    public static void main(String[] args) {
        ListaMembros membros = new ListaMembros();
        Thread t1 = new Thread(new Entregador(membros), "Entregador 01");
        Thread t2 = new Thread(new Entregador(membros), "Entregador 02");
        t1.start();
        t2.start();

        while (true) {
            String email = JOptionPane.showInputDialog("Digite o e-mail do membro");
            if (email == null || email.isEmpty()) {
                membros.fecharLista();
                break;
            }
            membros.adicionarEmailMembro(email);
        }
    }
}
