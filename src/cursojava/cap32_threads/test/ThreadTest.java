package cursojava.cap32_threads.test;

class ThreadExemplo extends Thread {
    private char c;

    public ThreadExemplo(char c) {
        this.c = c;
    }

    @Override
    public void run() {
        System.out.println("\nThread: " + Thread.currentThread().getName());
        for (int i = 0; i < 1000; i++) {
            System.out.print(c);
            if (i % 100 == 0) {
                System.out.println();
            }
        }
    }
}

class ThreadExemploRunnable implements Runnable {
    private char c;

    public ThreadExemploRunnable(char c) {
        this.c = c;
    }

    @Override
    public void run() {
        System.out.println("\nThread: " + Thread.currentThread().getName());
        for (int i = 0; i < 1000; i++) {
            System.out.print(c);
            if (i % 100 == 0) {
                System.out.println();
            }
//            Thread.yield();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


public class ThreadTest {
    public static void main(String[] args) throws InterruptedException {
        System.out.println(Thread.currentThread().getName());
//        ThreadExemplo t1 = new ThreadExemplo('A');
//        ThreadExemplo t2 = new ThreadExemplo('B');
//        ThreadExemplo t3 = new ThreadExemplo('C');
//        ThreadExemplo t4 = new ThreadExemplo('D');

//        O modo correto de iniciar uma Thread é usando o .start()
//        t1.start();
//        t2.start();
//        t3.start();
//        t4.start();

        Thread t1 = new Thread(new ThreadExemploRunnable('E'), "T1");
        Thread t2 = new Thread(new ThreadExemploRunnable('F'), "T2");
        Thread t3 = new Thread(new ThreadExemploRunnable('G'), "T3");
        Thread t4 = new Thread(new ThreadExemploRunnable('H'), "T4");

//        é possível definir a prioridade de uma thread numerando de 1 à 10
//        1 = menos importante / 10 = mais importante
//        no entanto, é recomendado utilizar Thread.(MIN | NORM | MAX)_PRIORITY
//        se um valor diferente for inserido, temos uma IllegalArgumentException
        t4.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t1.join();
        t2.start();
        t3.start();
        t4.start();

    }
}
