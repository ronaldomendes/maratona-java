package cursojava.cap33_concurrent.test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class BlockingQueueTest {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String> queue = new ArrayBlockingQueue<String>(1);
        Thread t1 = new Thread(new RemoveFromQueue(queue));
        queue.put("Godofredo");
        System.out.println(queue.peek());
        System.out.println("Tentando adicionar outro valor");
        t1.start();
        queue.put("Ronaldo, brilha muito!!!");
        System.out.println("Inserido o último valor");
    }

    static class RemoveFromQueue implements Runnable {
        private BlockingQueue<String> queue;

        public RemoveFromQueue(BlockingQueue<String> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " entrando em espera por 2 segundos");
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Removendo o valor: " + queue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
