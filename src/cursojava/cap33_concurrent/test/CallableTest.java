package cursojava.cap33_concurrent.test;

import java.util.concurrent.*;

public class CallableTest implements Callable<String> {
    public static void main(String[] args) {
        CallableTest test = new CallableTest();
        ExecutorService service = Executors.newCachedThreadPool();
        Future<String> future = service.submit(test);

        try {
            String str = future.get();
            System.out.println(str);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        service.shutdown();
    }

    @Override
    public String call() throws Exception {
        int count = ThreadLocalRandom.current().nextInt(1, 11);
        for (int i = 0; i < count; i++) {
            System.out.println(Thread.currentThread().getName() + " executando... ");
        }
        return "Trabalho finalizado... número aleatório: " + count;
    }
}
