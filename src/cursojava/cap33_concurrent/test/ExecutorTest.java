package cursojava.cap33_concurrent.test;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

class ThreadTrabalhadoraExecutor implements Runnable {
    private String num;

    public ThreadTrabalhadoraExecutor(String num) {
        this.num = num;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " iniciou: " + num);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " finalizou: " + num);
    }
}

public class ExecutorTest {
    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
//        limita o número de threads
//        ExecutorService service = Executors.newFixedThreadPool(2);
//        ExecutorService service = Executors.newCachedThreadPool();

//        essa função não permite a alteração na quantidade de threads (gera uma exception)
        ExecutorService service = Executors.newSingleThreadExecutor();

//        ThreadPoolExecutor poolExecutor = (ThreadPoolExecutor) service;
//        poolExecutor.setCorePoolSize(Runtime.getRuntime().availableProcessors());

        service.execute(new ThreadTrabalhadoraExecutor("1"));
        service.execute(new ThreadTrabalhadoraExecutor("2"));
        service.execute(new ThreadTrabalhadoraExecutor("3"));
        service.execute(new ThreadTrabalhadoraExecutor("4"));
        service.shutdown();

        while (!service.isTerminated()) {
        }
        System.out.println(service.isTerminated());
        System.out.println("Finalizando...");
    }
}
