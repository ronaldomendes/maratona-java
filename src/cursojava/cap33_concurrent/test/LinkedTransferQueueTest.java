package cursojava.cap33_concurrent.test;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;

public class LinkedTransferQueueTest {
    public static void main(String[] args) throws InterruptedException {
        TransferQueue<String> queue = new LinkedTransferQueue<>();
        System.out.println(queue.add("Pokémon"));
        queue.put("Jaspion");
        System.out.println(queue.offer("Jiraya"));
        System.out.println(queue.offer("Jiraya", 10, TimeUnit.MILLISECONDS));

        System.out.println(queue.tryTransfer("Jiban"));
        System.out.println(queue.tryTransfer("Jiban", 10, TimeUnit.MILLISECONDS));

        System.out.println(queue.remainingCapacity());

        System.out.println(queue.element());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.poll(10, TimeUnit.MILLISECONDS));
        System.out.println(queue.remove());
        System.out.println(queue.take());
    }
}
