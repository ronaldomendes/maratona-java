package cursojava.cap33_concurrent.test;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class MapReadWrite {
    private Map<String, String> map = new LinkedHashMap<>();

    public void put(String key, String value) {
        map.put(key, value);
    }

    public Object[] allKeys() {
        return map.keySet().toArray();
    }
}

public class MapReadWriteLockTest {
    private static final MapReadWrite MAP_READ_WRITE = new MapReadWrite();
    private static final ReentrantReadWriteLock READ_WRITE_LOCK = new ReentrantReadWriteLock();

    public static void main(String[] args) {
        Thread t1 = new Thread(new Write());
        Thread t2 = new Thread(new ReadA());
        Thread t3 = new Thread(new ReadB());

        t1.start();
        t2.start();
        t3.start();
    }

    static class Write implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 100; i++) {
                READ_WRITE_LOCK.writeLock().lock();
                try {
                    MAP_READ_WRITE.put(String.valueOf(i), String.valueOf(i));
                } finally {
                    READ_WRITE_LOCK.writeLock().unlock();
                }
            }
        }
    }

    static class ReadA implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                READ_WRITE_LOCK.readLock().lock();
                try {
                    System.out.println(Thread.currentThread().getName() + ": " + Arrays.toString(MAP_READ_WRITE.allKeys()));
                } finally {
                    READ_WRITE_LOCK.readLock().unlock();
                }
            }
        }
    }

    static class ReadB implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                READ_WRITE_LOCK.readLock().lock();
                try {
                    System.out.println(Thread.currentThread().getName() + ": " + Arrays.toString(MAP_READ_WRITE.allKeys()));
                } finally {
                    READ_WRITE_LOCK.readLock().unlock();
                }
            }
        }
    }
}
