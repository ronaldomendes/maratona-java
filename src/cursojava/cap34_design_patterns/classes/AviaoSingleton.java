package cursojava.cap34_design_patterns.classes;

import java.util.HashSet;
import java.util.Set;

public class AviaoSingleton {
//    EAGER INITIALIZATION
//    private static final AviaoSingleton INSTANCE = new AviaoSingleton();


    //    LAZY INITIALIZATION
    private static AviaoSingleton INSTANCE;
    private Set<String> assentosDisponiveis;

    private AviaoSingleton() {
        this.assentosDisponiveis = new HashSet<>();
        assentosDisponiveis.add("1A");
        assentosDisponiveis.add("1B");
    }

    public static AviaoSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new AviaoSingleton();
            synchronized (AviaoSingleton.class) {
                if (INSTANCE == null) {
                    INSTANCE = new AviaoSingleton();
                }
            }
        }
        return INSTANCE;
    }

    public boolean bookAssento(String assento) {
        return assentosDisponiveis.remove(assento);
    }
}
