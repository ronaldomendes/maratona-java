package cursojava.cap34_design_patterns.test;

import cursojava.cap34_design_patterns.classes.AviaoSingleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class AviaoSingletonTest {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        agendarAssento("1A");
        agendarAssento("1A");

        AviaoSingleton aviao01 = AviaoSingleton.getINSTANCE();
        AviaoSingleton aviao02 = null;

        Constructor[] constructors = AviaoSingleton.class.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            constructor.setAccessible(true);
            aviao02 = (AviaoSingleton) constructor.newInstance();
            break;
        }

        System.out.println(aviao01.hashCode());
        System.out.println(aviao02.hashCode());
    }

    private static void agendarAssento(String assento) {
        AviaoSingleton aviao = AviaoSingleton.getINSTANCE();
        System.out.println(aviao.bookAssento(assento));
    }
}
