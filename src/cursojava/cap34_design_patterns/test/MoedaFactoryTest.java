package cursojava.cap34_design_patterns.test;

import cursojava.cap34_design_patterns.classes.Moeda;
import cursojava.cap34_design_patterns.classes.MoedaFactory;
import cursojava.cap34_design_patterns.classes.Pais;

public class MoedaFactoryTest {
    public static void main(String[] args) {
        Moeda moeda01 = MoedaFactory.criarMoeda(Pais.BRASIL);
        System.out.println(moeda01.getSimbolo());

        Moeda moeda02 = MoedaFactory.criarMoeda(Pais.EUA);
        System.out.println(moeda02.getSimbolo());
    }
}
