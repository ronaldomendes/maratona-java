package cursojava.cap34_design_patterns.test;

import cursojava.cap34_design_patterns.classes.Pessoa;

public class PessoaTest {
    public static void main(String[] args) {
//        Pessoa pessoa = new Pessoa("Godofredo", "Silva", "Santos", "Godo", "Tonho");
        Pessoa pessoa = new Pessoa.PessoaBuilder()
                .nome("Godofredo")
                .ultimoNome("Silva")
                .nomeDoMeio("Santos")
                .apelido("Godo")
                .nomeDoPai("Tonho")
                .build();

        System.out.println(pessoa);
    }
}
