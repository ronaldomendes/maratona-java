package cursojava.cap35_behavior_parameterization.interfaces;

import cursojava.cap35_behavior_parameterization.classes.Carro;

public interface CarroPredicate {
    boolean test(Carro carro);
}
