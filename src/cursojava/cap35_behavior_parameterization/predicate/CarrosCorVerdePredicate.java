package cursojava.cap35_behavior_parameterization.predicate;

import cursojava.cap35_behavior_parameterization.classes.Carro;
import cursojava.cap35_behavior_parameterization.interfaces.CarroPredicate;

public class CarrosCorVerdePredicate implements CarroPredicate {
    @Override
    public boolean test(Carro carro) {
        return carro.getCor().equals("Verde");
    }
}
