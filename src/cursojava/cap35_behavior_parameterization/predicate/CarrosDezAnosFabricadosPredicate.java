package cursojava.cap35_behavior_parameterization.predicate;

import cursojava.cap35_behavior_parameterization.classes.Carro;
import cursojava.cap35_behavior_parameterization.interfaces.CarroPredicate;

import java.util.Calendar;

public class CarrosDezAnosFabricadosPredicate implements CarroPredicate {
    @Override
    public boolean test(Carro carro) {
        return carro.getAno() > (Calendar.getInstance().get(Calendar.YEAR) - 10);
    }
}
