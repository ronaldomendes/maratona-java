package cursojava.cap35_behavior_parameterization.test;

import cursojava.cap35_behavior_parameterization.classes.Carro;
import cursojava.cap35_behavior_parameterization.interfaces.CarroPredicate;
import cursojava.cap35_behavior_parameterization.predicate.CarrosCorVerdePredicate;
import cursojava.cap35_behavior_parameterization.predicate.CarrosDezAnosFabricadosPredicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.function.Predicate;

public class CarroTest {
    private static List<Carro> filtrarCarroPorCor(List<Carro> carros, String cor) {
        List<Carro> result = new ArrayList<>();
        for (Carro carro : carros) {
            if (carro.getCor().equals(cor)) result.add(carro);
        }
        return result;
    }

    private static List<Carro> filtrarCarroDezAnosFabricados(List<Carro> carros) {
        List<Carro> result = new ArrayList<>();
        for (Carro carro : carros) {
            if (carro.getAno() > (Calendar.getInstance().get(Calendar.YEAR) - 10)) {
                result.add(carro);
            }
        }
        return result;
    }

    public static List<Carro> filtrarCarros(List<Carro> carros, CarroPredicate predicate) {
        List<Carro> result = new ArrayList<>();
        for (Carro carro : carros) {
            if (predicate.test(carro)) result.add(carro);
        }
        return result;
    }

    public static <T> List<T> filtrar(List<T> list, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T t : list) {
            if (predicate.test(t)) result.add(t);
        }
        return result;
    }

    public static void main(String[] args) {
        List<Carro> carros = Arrays.asList(
                new Carro("Verde", 2011),
                new Carro("Azul", 1999),
                new Carro("Amarelo", 2018),
                new Carro("Preto", 2019),
                new Carro("Verde", 1994));

        List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

//        System.out.println(filtrarCarroPorCor(carros, "Amarelo"));
//        System.out.println(filtrarCarroPorCor(carros, "Verde"));
//        System.out.println(filtrarCarroDezAnosFabricados(carros));

        System.out.println(filtrarCarros(carros, new CarrosCorVerdePredicate()));
        System.out.println(filtrarCarros(carros, new CarrosDezAnosFabricadosPredicate()));
        System.out.println(filtrarCarros(carros, carro -> carro.getCor().equals("Preto")));
        System.out.println(filtrar(carros, carro -> carro.getCor().equals("Amarelo")));
        System.out.println(filtrar(nums, integer -> integer % 2 == 0));

//        System.out.println(filtrarCarros(carros, new CarroPredicate() {
//            @Override
//            public boolean test(Carro carro) {
//                return carro.getCor().equals("Preto");
//            }
//        }));
//
//        System.out.println(filtrar(carros, new Predicate<Carro>() {
//            @Override
//            public boolean test(Carro carro) {
//                return carro.getCor().equals("Amarelo");
//            }
//        }));
//
//
//        System.out.println(filtrar(nums, new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                return integer % 2 == 0;
//            }
//        }));
    }
}
