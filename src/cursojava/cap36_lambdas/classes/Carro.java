package cursojava.cap36_lambdas.classes;

public class Carro {
    private String nome = "Kombi";
    private String cor;
    private int ano;

    public Carro(String cor, int ano) {
        this.cor = cor;
        this.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Carro{");
        sb.append("nome='").append(nome).append('\'');
        sb.append(", cor='").append(cor).append('\'');
        sb.append(", ano=").append(ano);
        sb.append('}');
        return sb.toString();
    }
}
