package cursojava.cap36_lambdas.interfaces;


import cursojava.cap36_lambdas.classes.Carro;

//é um tipo de interface com apenas um método
@FunctionalInterface
public interface CarroPredicate {
    boolean test(Carro carro);
}
