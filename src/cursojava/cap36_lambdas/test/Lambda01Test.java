package cursojava.cap36_lambdas.test;

import cursojava.cap36_lambdas.classes.Carro;
import cursojava.cap36_lambdas.interfaces.CarroPredicate;

public class Lambda01Test {
    public static void main(String[] args) {
        CarroPredicate carro01 = new CarroPredicate() {
            @Override
            public boolean test(Carro carro) {
                return carro.getCor().equals("Verde");
            }
        };

        CarroPredicate carro02 = (Carro carro) -> carro.getCor().equals("Verde");
        System.out.println(carro01.test(new Carro("Verde", 2020)));
        System.out.println(carro02.test(new Carro("Preto", 2000)));

        Runnable runnable = () -> System.out.println("Dentro do run");
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
