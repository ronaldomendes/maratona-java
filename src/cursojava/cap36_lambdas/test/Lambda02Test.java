package cursojava.cap36_lambdas.test;

import cursojava.cap36_lambdas.classes.Carro;

import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.*;

public class Lambda02Test {
    public static void main(String[] args) throws Exception {
        forEach(Arrays.asList("Café", "Cigarro", "Coca-Cola", "Stress"), (String s) -> System.out.println(s));
        List<Integer> list = map(Arrays.asList("Café", "Cigarro", "Coca-Cola", "Stress"), (String s) -> s.length());
        List<Carro> carros = Arrays.asList(new Carro("Preto", 2000), new Carro("Azul", 2000),
                new Carro("Preto", 2000), new Carro("Azul", 2000), new Carro("Preto", 2000));
        List<String> listCores = map(carros, (Carro c) -> c.getCor());
        System.out.println(list);
        System.out.println(listCores);

        Predicate<Integer> pares = (Integer i) -> i % 2 == 0;
        IntPredicate impares = (int i) -> i % 2 == 1;
        System.out.println(pares.test(1000));
        System.out.println(impares.test(1000));

        Callable<Integer> callable = () -> 100;
        PrivilegedAction<Integer> privilegedAction = () -> 100;
        System.out.println(callable.call());
        System.out.println(privilegedAction.run());

        Predicate<String> stringPredicate = (String s) -> listCores.add(s);
        stringPredicate.test("Roxo");
        Consumer<String> stringConsumer = listCores::add;
        stringConsumer.accept("Amarelo");
        System.out.println(listCores);

        Supplier<String> stringSupplier = () -> "Olá!";
        Supplier<Carro> carroSupplier = () -> new Carro("Verde", 1999);
        System.out.println(stringSupplier.get());
        System.out.println(carroSupplier.get().getAno());
    }

    public static <T> void forEach(List<T> list, Consumer<T> consumer) {
        for (T t : list) {
            consumer.accept(t);
        }
    }

    public static <T, R> List<R> map(List<T> list, Function<T, R> function) {
        List<R> result = new ArrayList<>();
        for (T t : list) {
            result.add(function.apply(t));
        }
        return result;
    }
}
