package cursojava.cap36_lambdas.test;

import cursojava.cap36_lambdas.classes.Carro;
import cursojava.cap36_lambdas.classes.ComparadorCarro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.*;

public class Lambda03Test {
    public static void main(String[] args) {
        forEach(Arrays.asList("Café", "Cigarro", "Coca-Cola", "Stress"), System.out::println);
        List<Carro> carros = Arrays.asList(new Carro("Preto", 1999), new Carro("Roxo", 2010),
                new Carro("Vermelho", 2003), new Carro("Azul", 1995), new Carro("Preto", 2020));

//        Reference to a static method
        Collections.sort(carros, ComparadorCarro::comparePorCor);
        System.out.println(carros);

//        Reference to an instance method of a particular object
        ComparadorCarro comparador = new ComparadorCarro();
        Collections.sort(carros, comparador::comparePorAno);
        System.out.println(carros);

//        Reference to an instance method of an arbitrary object of a particular type
        List<String> nomes = Arrays.asList("Chilli Willy", "Pica Pau", "Leoncio", "Zeca Urubu", "Andy Panda");
//        nomes.sort((n1, n2) -> n1.compareTo(n2));
        nomes.sort(String::compareTo);
        System.out.println(nomes);

        Function<String, Integer> stringIntegerFunction = (String s) -> Integer.parseInt(s);
        Function<String, Integer> stringIntegerLambdaReference = Integer::parseInt;

        BiPredicate<List<String>, String> contains = (lista, elemento) -> lista.contains(elemento);
        BiPredicate<List<String>, String> containsLambdaReference = List::contains;
        System.out.println(stringIntegerFunction.apply("100"));
        System.out.println(containsLambdaReference.test(nomes, "Zeca Urubu"));

//        Reference to a constructor
        Supplier<ComparadorCarro> comparadorCarroSupplier = ComparadorCarro::new;
        List<Carro> carroList = Arrays.asList(new Carro("Preto", 1999), new Carro("Roxo", 2010),
                new Carro("Vermelho", 2003), new Carro("Azul", 1995), new Carro("Preto", 2020));
        carroList.sort(comparadorCarroSupplier.get()::comparePorAno);
        System.out.println(carroList);

        BiFunction<String, Integer, Carro> carroBiFunction = (s, i) -> new Carro(s, i);
        BiFunction<String, Integer, Carro> carroBiFunctionLambdaReference = Carro::new;
        System.out.println(carroBiFunction.apply("Verde", 1954));
        System.out.println(carroBiFunctionLambdaReference.apply("Amarelo", 1988));
    }

    public static <T> void forEach(List<T> list, Consumer<T> consumer) {
        for (T t : list) {
            consumer.accept(t);
        }
    }

    public static <T, R> List<R> map(List<T> list, Function<T, R> function) {
        List<R> result = new ArrayList<>();
        for (T t : list) {
            result.add(function.apply(t));
        }
        return result;
    }
}
