package cursojava.cap37_default_methods.interfaces;

public interface A {
    default void oi() {
        System.out.println("Dentro do oi de A");
    }
}
