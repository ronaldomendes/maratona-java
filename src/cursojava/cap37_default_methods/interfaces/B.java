package cursojava.cap37_default_methods.interfaces;

public interface B {
    default void oi() {
        System.out.println("Dentro do oi de B");
    }
}
