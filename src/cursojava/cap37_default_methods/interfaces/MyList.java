package cursojava.cap37_default_methods.interfaces;

public interface MyList {
    static void sort() {
        System.out.println("Dentro do método sort");
    }

    void add();

    default void remove() {
        System.out.println("Dentro do remove");
    }

    ;
}
