package cursojava.cap37_default_methods.test;

import cursojava.cap37_default_methods.interfaces.A;
import cursojava.cap37_default_methods.interfaces.B;
import cursojava.cap37_default_methods.interfaces.D;

public class C implements A, B {
    public static void main(String[] args) {
        new C().oi();
    }

    public void oi() {
        A.super.oi();
    }
}
