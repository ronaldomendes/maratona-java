package cursojava.cap37_default_methods.test;

import cursojava.cap37_default_methods.interfaces.MyList;

public class InterfaceTest implements MyList {
    public static void main(String[] args) {
        MyList.sort();
        new InterfaceTest().remove();
    }

    @Override
    public void add() {
        System.out.println("Dentro do add");
    }
}
