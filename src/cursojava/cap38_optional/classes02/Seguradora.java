package cursojava.cap38_optional.classes02;

public class Seguradora {
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
