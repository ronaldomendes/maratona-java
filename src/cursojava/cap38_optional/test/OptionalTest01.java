package cursojava.cap38_optional.test;

import java.util.Optional;

public class OptionalTest01 {
    private String nome;
    public static void main(String[] args) {
        OptionalTest01 test01 = new OptionalTest01();
        Optional<String> optional01 = Optional.of("test01.nome");
        Optional<String> optional02 = Optional.empty();
        Optional<String> optional03 = Optional.ofNullable(test01.nome);

//        imprimindo
        System.out.println(optional01);
        System.out.println(optional02);
        System.out.println(optional03);

//        buscando valores
        System.out.println(optional01.orElse("vazio"));
        System.out.println(optional02.orElse("vazio"));
        System.out.println(optional03.orElse("vazio"));
    }
}
