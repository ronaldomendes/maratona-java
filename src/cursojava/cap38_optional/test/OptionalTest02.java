package cursojava.cap38_optional.test;

import cursojava.cap38_optional.classes01.Carro;
import cursojava.cap38_optional.classes01.Pessoa;
import cursojava.cap38_optional.classes01.Seguradora;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OptionalTest02 {
    public static void main(String[] args) {
        Seguradora seguradora = new Seguradora("Tabajara");
        Carro carro = new Carro(seguradora, "Fiat 147");
//        carro = null;
        Pessoa pessoa = new Pessoa(carro, "Godofredo");

        obterNomeSeguradora(Optional.ofNullable(pessoa));
        checarNomeSeguradora(seguradora);
        checarNomeSeguradoraOptional(seguradora);

        System.out.println(obterNomeSeguradoraComIdade(pessoa, 18));
        pessoa.setIdade(20);
        System.out.println(obterNomeSeguradoraComIdade(pessoa, 18));

        Map<String, String> map = new HashMap<>();
        System.out.println(Optional.ofNullable(map.get("bla")));

        stringToInt("A");
    }

    private static Optional<Integer> stringToInt(String numero) {
        try {
            return Optional.of(Integer.parseInt(numero));
        } catch (NumberFormatException e) {
//            e.printStackTrace();
            return Optional.empty();
        }
    }

    private static void checarNomeSeguradora(Seguradora seguradora) {
        if (seguradora != null && seguradora.getNome().equals("Tabajara")) {
            System.out.println("A seguradora está correta...");
        }
    }

    private static void checarNomeSeguradoraOptional(Seguradora seguradora) {
        Optional.ofNullable(seguradora).filter(seg -> seg.getNome().equals("Tabajara"))
                .ifPresent(x -> System.out.println("Acertou, mizeravi"));
    }

    private static String obterNomeSeguradora(Optional<Pessoa> pessoa) {
        System.out.println(pessoa.flatMap(Pessoa::getCarro));
        System.out.println(pessoa.flatMap(Pessoa::getCarro).flatMap(Carro::getSeguradora));
        System.out.println(pessoa.flatMap(Pessoa::getCarro).map(Carro::getSeguradora));
        System.out.println(pessoa.flatMap(Pessoa::getCarro).flatMap(Carro::getSeguradora)
                .map(Seguradora::getNome).orElse("Vazio..."));

        return pessoa.flatMap(Pessoa::getCarro).flatMap(Carro::getSeguradora)
                .map(Seguradora::getNome).orElse("Vazio...");
    }

//    private static String obterNomeSeguradoraOptional(Seguradora seguradora) {
//        System.out.println(Optional.ofNullable(seguradora).flatMap(Seguradora::getNome).orElse("vazio"));
//        System.out.println(Optional.ofNullable(seguradora).map(Seguradora::getNome));
//        return "";
//    }

    private static String obterNomeSeguradora(Seguradora seguradora) {
        String nome = null;
        if (seguradora.getNome() != null) {
//            nome = seguradora.getNome();
        }
        return nome;
    }

    private static String obterNomeSeguradoraComIdade(Pessoa pessoa, int idadeMinima) {
        return Optional.ofNullable(pessoa).filter(p -> p.getIdade() >= idadeMinima)
                .flatMap(Pessoa::getCarro).flatMap(Carro::getSeguradora).map(Seguradora::getNome)
                .orElse("Não existe seguradora ou a idade mínina não foi atingida");
    }
}
