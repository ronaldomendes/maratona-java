package cursojava.cap39_streams.classes;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Pessoa {
    private String nome;
    private int idade;
    private double salario;
    private Genero genero;

    public Pessoa(String nome, int idade, double salario) {
        this.nome = nome;
        this.idade = idade;
        this.salario = salario;
    }

    public Pessoa(String nome, int idade, double salario, Genero genero) {
        this.nome = nome;
        this.idade = idade;
        this.salario = salario;
        this.genero = genero;
    }

    public static List<Pessoa> bancoDePessoas() {
        return Arrays.asList(
                new Pessoa("Venom", 17, 5000, Genero.MASCULINO),
                new Pessoa("Ryu", 43, 3500, Genero.MASCULINO),
                new Pessoa("Akuma", 22, 1985, Genero.MASCULINO),
                new Pessoa("Spider-Man", 33, 1900, Genero.MASCULINO),
                new Pessoa("Psylocke", 41, 3000, Genero.FEMININO),
                new Pessoa("Chun-Li", 17, 2000, Genero.FEMININO),
                new Pessoa("Hulk", 29, 8000, Genero.MASCULINO),
                new Pessoa("War Machine", 30, 3180, Genero.MASCULINO),
                new Pessoa("Dhalsim", 27, 9000, Genero.MASCULINO),
                new Pessoa("Zangief", 39, 6000, Genero.MASCULINO)
        );
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Genero getGenero() {
        return genero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pessoa pessoa = (Pessoa) o;
        return Objects.equals(nome, pessoa.nome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pessoa{");
        sb.append("nome='").append(nome).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
