package cursojava.cap39_streams.test;

import cursojava.cap39_streams.classes.Pessoa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamTest01 {
    public static void main(String[] args) {
        // pegar os tres primeiros nomes das pessoas com menos de 25 anos, ordenados pelo nome
        List<Pessoa> pessoas = Pessoa.bancoDePessoas();
        Collections.sort(pessoas, (o1, o2) -> o1.getNome().compareTo(o2.getNome()));

        List<String> nomes = new ArrayList<>();
        for (Pessoa pessoa : pessoas) {
            if (pessoa.getIdade() < 30) {
                nomes.add(pessoa.getNome());
                if (nomes.size() >= 3) break;
            }
        }
        System.out.println(nomes);

        List<String> nomesStream = pessoas.stream().filter(p -> p.getIdade() < 30)
                .sorted(Comparator.comparing(Pessoa::getNome))
                .limit(4).skip(1)
                .map(Pessoa::getNome).collect(Collectors.toList());
        System.out.println(nomesStream);

        System.out.println(pessoas.stream().filter(p -> p.getIdade() < 30).map(Pessoa::getNome).count());
        System.out.println(pessoas.stream().distinct().filter(p -> p.getIdade() < 30).map(Pessoa::getNome).count());
        pessoas.forEach(System.out::println);
    }
}
