package cursojava.cap39_streams.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest02 {
    public static void main(String[] args) {
        List<List<String>> nomes = new ArrayList<>();
        nomes.add(Arrays.asList("Ronaldo", "Brilha muito no Corinthians"));
        nomes.add(Arrays.asList("Pica-Pau", "Vodu é pra jacu"));

        Stream<String> stringStream = nomes.stream().flatMap(Collection::stream);
        stringStream.forEach(System.out::println);

        List<String> collect = nomes.stream().flatMap(Collection::stream).collect(Collectors.toList());
        collect.forEach(System.out::println);

        System.out.println(nomes);

        List<String> palavras = Arrays.asList("Olá", "Mundo");
        String[] split = palavras.get(0).split("");
        System.out.println(Arrays.toString(split));

        List<String[]> strings = palavras.stream().map(p -> p.split("")).collect(Collectors.toList());
        Stream<Object> stream = Arrays.stream(palavras.toArray());

        List<String> list = palavras.stream().map(p -> p.split(""))
                .flatMap(Arrays::stream).collect(Collectors.toList());
        System.out.println(list);
    }
}
