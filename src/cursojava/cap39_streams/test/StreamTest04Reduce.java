package cursojava.cap39_streams.test;

import cursojava.cap39_streams.classes.Pessoa;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class StreamTest04Reduce {
    public static void main(String[] args) {
        Stream<Integer> stream = getStream();
        Optional<Integer> integer = stream.reduce((x, y) -> x + y);
        System.out.println(integer.get());

        Integer soma01 = getStream().reduce(0, (x, y) -> x + y);
        System.out.println(soma01);

        stream = getStream();
        Optional<Integer> soma02 = stream.reduce(Integer::sum);
        System.out.println(soma02.get());

        Integer soma03 = getStream().reduce(0, Integer::sum);
        System.out.println(soma03);

        Optional<Integer> mult = getStream().reduce((x, y) -> x * y);
        System.out.println(mult.get());

        Optional<Integer> max01 = getStream().reduce((x, y) -> x > y ? x : y);
        System.out.println(max01.get());

        Optional<Integer> max02 = getStream().reduce(Integer::max);
        System.out.println(max02.get());

        Stream<Pessoa> pessoaStream = Pessoa.bancoDePessoas().stream();
        Optional<Double> somaSalarios = pessoaStream.filter(p -> p.getSalario() > 4000)
                .map(Pessoa::getSalario).reduce(Double::sum);
        System.out.println(somaSalarios.get());

        Double somaDouble = Pessoa.bancoDePessoas().stream().filter(p -> p.getSalario() > 4000)
                .mapToDouble(Pessoa::getSalario).sum();
        System.out.println(somaDouble);

        DoubleStream doubleStream = Pessoa.bancoDePessoas().stream().filter(p -> p.getSalario() > 4000)
                .mapToDouble(Pessoa::getSalario);
        Stream<Double> newDoubleStream = doubleStream.boxed();
    }

    private static Stream<Integer> getStream() {
//        return Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9).stream();
        return Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
    }
}
