package cursojava.cap39_streams.test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest05GeneratingStreams {
    public static void main(String[] args) {
        IntStream.rangeClosed(1, 50).filter(n -> n % 2 == 0).forEach(s -> System.out.print(s + " "));
        System.out.println();
        IntStream.range(1, 50).filter(n -> n % 2 == 0).forEach(s -> System.out.print(s + " "));

        Stream<String> stringStream = Stream.of("Java", "C#", "Python", "JavaScript", "PHP");
        System.out.println();
        stringStream.map(String::toUpperCase).forEach(s -> System.out.print(s + " "));
        Stream<String> empty = Stream.empty();

        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        OptionalDouble avg = Arrays.stream(nums).average();
        System.out.println("\n" + avg.getAsDouble());

        try (Stream<String> lines = Files.lines(Paths.get("teste.txt"), Charset.defaultCharset())) {
            lines.flatMap(line -> Arrays.stream(line.split("\n")))
                    .filter(p -> p.contains("File")).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stream.iterate(1, n -> n + 2).limit(10).forEach(s -> System.out.print(s + " "));
        System.out.println("\nSequência de Fibonacci");
        Stream.iterate(new int[]{0, 1}, a -> new int[]{a[1], a[0] + a[1]}).limit(20)
                .map(s -> s[0]) // pegando apenas a primeira posição do array
//                .forEach(s -> System.out.print(Arrays.toString(s) + " ")); // usando o .map, temos um erro nessa linha
                .forEach(s -> System.out.print(s + " "));

        System.out.println();
        Stream.generate(Math::random).limit(10).forEach(s -> System.out.print(s + " "));
        System.out.println();
        ThreadLocalRandom localRandom = ThreadLocalRandom.current();
        Stream.generate(() -> localRandom.nextInt(1, 100)).limit(100).forEach(s -> System.out.print(s + " "));
    }
}
