package cursojava.cap39_streams.test;

import cursojava.cap39_streams.classes.Pessoa;

import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamTest06Collectors01 {
//        Redução e sumarização de streams em um valor único
    //        Particionamento de elementos
    public static void main(String[] args) {
        List<Pessoa> pessoas = Pessoa.bancoDePessoas();
        System.out.println(pessoas.stream().count());
        System.out.println(pessoas.stream().collect(Collectors.counting()));

        Optional<Pessoa> max = pessoas.stream().max(Comparator.comparing(Pessoa::getSalario));
        System.out.println(max.get().getSalario());

        Optional<Pessoa> maxCollect = pessoas.stream().collect(Collectors.maxBy(Comparator.comparing(Pessoa::getSalario)));
        System.out.println(maxCollect.get().getSalario());

        Optional<Pessoa> minCollect = pessoas.stream().collect(Collectors.minBy(Comparator.comparing(Pessoa::getSalario)));
        System.out.println(minCollect.get().getSalario());

        System.out.println(pessoas.stream().mapToDouble(Pessoa::getSalario).sum());
        System.out.println(pessoas.stream().collect(Collectors.summingDouble(Pessoa::getSalario)));

        System.out.println(pessoas.stream().mapToDouble(Pessoa::getSalario).average());
        System.out.println(pessoas.stream().collect(Collectors.averagingDouble(Pessoa::getSalario)));

        DoubleSummaryStatistics collect = pessoas.stream().collect(Collectors.summarizingDouble(Pessoa::getSalario));
        System.out.println(collect.getAverage());

        System.out.println(pessoas.stream().map(Pessoa::getNome).collect(Collectors.joining()));
        System.out.println(pessoas.stream().map(Pessoa::getNome).collect(Collectors.joining(", ")));
        System.out.println(pessoas.stream().map(Pessoa::getNome).sorted()
                .collect(Collectors.joining(", ")));
    }
}
