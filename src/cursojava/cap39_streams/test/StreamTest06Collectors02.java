package cursojava.cap39_streams.test;

import cursojava.cap39_streams.classes.Genero;
import cursojava.cap39_streams.classes.Maioridade;
import cursojava.cap39_streams.classes.Pessoa;

import java.util.*;
import java.util.stream.Collectors;

public class StreamTest06Collectors02 {
    //    Agrupamento de elementos
    public static void main(String[] args) {
        List<Pessoa> pessoas = Pessoa.bancoDePessoas();

//        Início - Modo 01 (Java 7)
        Map<Genero, List<Pessoa>> generoListMap = new HashMap<>();
        List<Pessoa> masculino = new ArrayList<>();
        List<Pessoa> feminino = new ArrayList<>();

        for (Pessoa pessoa : pessoas) {
            if (pessoa.getGenero().equals(Genero.MASCULINO)) {
                masculino.add(pessoa);
            } else {
                feminino.add(pessoa);
            }
        }
        generoListMap.put(Genero.FEMININO, feminino);
        generoListMap.put(Genero.MASCULINO, masculino);
//        Fim
        System.out.println(generoListMap);

//        Início - Modo 02 (Java 8)
//        Agrupando por gênero em ordem alfabética
        Map<Genero, List<Pessoa>> collect = pessoas.stream()
                .sorted(Comparator.comparing(Pessoa::getNome)) // adicionei essa linha para ordenar por nome
                .collect(Collectors.groupingBy(Pessoa::getGenero));
        System.out.println(collect);

//        Agrupando por maioridade
        Map<Maioridade, List<Pessoa>> collect1 = pessoas.stream()
                .sorted(Comparator.comparing(Pessoa::getNome))
                .collect(Collectors.groupingBy(p -> (p.getIdade() < 18) ? Maioridade.MENOR : Maioridade.ADULTO));
        System.out.println(collect1);

//        Agrupando por gênero
        Map<Genero, Map<Maioridade, List<Pessoa>>> collect2 = pessoas.stream()
                .sorted(Comparator.comparing(Pessoa::getNome)).collect(Collectors.groupingBy(Pessoa::getGenero,
                        Collectors.groupingBy(p -> (p.getIdade() < 18) ? Maioridade.MENOR : Maioridade.ADULTO)));
        System.out.println(collect2);

//        Agrupando por gênero e quantidade
        Map<Genero, Long> collect3 = pessoas.stream().collect(Collectors
                .groupingBy(Pessoa::getGenero, Collectors.counting()));
        System.out.println(collect3);

//        Agrupando por gênero e maior salário com Optional
        Map<Genero, Optional<Pessoa>> collect4 = pessoas.stream().sorted(Comparator.comparing(Pessoa::getNome))
                .collect(Collectors.groupingBy(Pessoa::getGenero,
                        Collectors.maxBy(Comparator.comparing(Pessoa::getSalario))));
        System.out.println(collect4);

//        Agrupando por gênero e maior salário sem Optional
        Map<Genero, Pessoa> collect5 = pessoas.stream().collect(Collectors.groupingBy(Pessoa::getGenero,
                Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparing(Pessoa::getSalario)),
                        Optional::get)));
        System.out.println(collect5);

//        Agrupando por gênero e estatísticas
        Map<Genero, DoubleSummaryStatistics> collect6 = pessoas.stream()
                .collect(Collectors.groupingBy(Pessoa::getGenero, Collectors.summarizingDouble(Pessoa::getSalario)));
        System.out.println(collect6);

//        Agrupando por gênero e maioridade
        Map<Genero, Set<Maioridade>> collect7 = pessoas.stream().collect(Collectors
                .groupingBy(Pessoa::getGenero, Collectors.mapping(p -> {
                    if (p.getIdade() < 18) return Maioridade.MENOR;
                    else return Maioridade.ADULTO;
                }, Collectors.toSet())));
        System.out.println(collect7);

        Map<Genero, Set<Maioridade>> collect8 = pessoas.stream().collect(Collectors
                .groupingBy(Pessoa::getGenero, Collectors.mapping(p -> {
                    if (p.getIdade() < 18) return Maioridade.MENOR;
                    else return Maioridade.ADULTO;
                }, Collectors.toCollection(LinkedHashSet::new))));
        System.out.println(collect8);
    }
}
