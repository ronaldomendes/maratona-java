package cursojava.cap41_completable_future.classes;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Desconto {
    public enum Codigo {
        NENHUM(0),
        SILVER(5),
        GOLD(10),
        PLATINUM(15),
        ELITE(20);

        private final int porcentagem;

        Codigo(int porcentagem) {
            this.porcentagem = porcentagem;
        }

        public int getPorcentagem() {
            return porcentagem;
        }
    }

    public static String calcularDesconto(Orcamento o) {
        return String.format("%s preço original: %.2f, desconto: %d, preço final: %.2f",
                o.getNomeLoja(), o.getPreco(), o.getCodigoDesconto().getPorcentagem(),
                calculo(o.getPreco(), o.getCodigoDesconto()));
    }

    private static double calculo(double preco, Codigo codigo) {
        delay();
        return preco * (100 - codigo.porcentagem) / 100;
    }

    private static void delay() {
        try {
            int delay = ThreadLocalRandom.current().nextInt(500, 2000);
            TimeUnit.MILLISECONDS.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
