package cursojava.cap41_completable_future.classes;

public class Orcamento {
    private final String nomeLoja;
    private final double preco;
    private final Desconto.Codigo codigoDesconto;

    private Orcamento(String nomeLoja, double preco, Desconto.Codigo codigoDesconto) {
        this.nomeLoja = nomeLoja;
        this.preco = preco;
        this.codigoDesconto = codigoDesconto;
    }

    public static Orcamento parse(String s) {
        String[] split = s.split(":");
        String loja = split[0];
        double preco = Double.parseDouble(split[1].replace(",", "."));
        Desconto.Codigo codigo = Desconto.Codigo.valueOf(split[2]);
        return new Orcamento(loja, preco, codigo);
    }

    public String getNomeLoja() {
        return nomeLoja;
    }

    public double getPreco() {
        return preco;
    }

    public Desconto.Codigo getCodigoDesconto() {
        return codigoDesconto;
    }
}
