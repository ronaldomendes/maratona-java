package cursojava.cap41_completable_future.test;

import java.util.concurrent.*;

public class FutureTest {
    private static ExecutorService service = Executors.newFixedThreadPool(1);

    public static void main(String[] args) {
        Future<Double> doubleFuture = service.submit(() -> {
            TimeUnit.SECONDS.sleep(2);
            return 2000D;
        });
        enrolando();
        try {
            while (!doubleFuture.isDone()) {
                Double result = doubleFuture.get(3, TimeUnit.SECONDS);
                System.out.println(result);
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }
    }

    public static void enrolando() {
        long soma = 0;
        for (int i = 0; i < 1000000; i++) {
            soma += i;
        }
        System.out.println(soma);
    }
}
