package cursojava.cap41_completable_future.test;

import cursojava.cap41_completable_future.classes.Loja;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class LojaTest01 {
    public static void main(String[] args) {
        Loja americanas = new Loja();
        Loja submarino = new Loja();
        Loja decathlon = new Loja();
        Loja kalunga = new Loja();

        long inicio = System.currentTimeMillis();

//        pegando o preço de forma sincrona
//        System.out.println(americanas.getPreco());
//        System.out.println(submarino.getPreco());
//        System.out.println(decathlon.getPreco());
//        System.out.println(kalunga.getPreco());

//        pegando o preço de forma assincrona
       Future<Double> future01 = americanas.getPrecoSupplyAsync();
       Future<Double> future02 = submarino.getPrecoSupplyAsync();
       Future<Double> future03 = decathlon.getPrecoSupplyAsync();
       Future<Double> future04 = kalunga.getPrecoSupplyAsync();

        long fim = System.currentTimeMillis();
        System.out.println(fim - inicio + " ms");
        FutureTest.enrolando();

        try {
            System.out.println("americanas " + future01.get());
            System.out.println("submarino " + future02.get());
            System.out.println("decathlon " + future03.get());
            System.out.println("kalunga " + future04.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        long fim_mesmo = System.currentTimeMillis();
        System.out.println("Tempo total: " + (fim_mesmo - inicio) + " ms");
    }
}
