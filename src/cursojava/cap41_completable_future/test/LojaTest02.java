package cursojava.cap41_completable_future.test;

import cursojava.cap41_completable_future.classes.Loja;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class LojaTest02 {
    public static void main(String[] args) {
        List<Loja> lojas = Arrays.asList(
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"),
                new Loja("americanas"), new Loja("submarino"), new Loja("decathlon"),
                new Loja("kalunga"), new Loja("americanas"), new Loja("submarino"),
                new Loja("decathlon"), new Loja("kalunga"), new Loja("americanas"),
                new Loja("submarino"), new Loja("decathlon"), new Loja("kalunga"));
        System.out.println(Runtime.getRuntime().availableProcessors());
        System.out.println(lojas.size());
//        acharPrecosStream(lojas);
        acharPrecosParallelStream(lojas);
//        acharPrecosCompletableFutureSeq(lojas);
        acharPrecosCompletableFuture(lojas);

//        número de threads = ncpu * ucpu * (1 + W / C)
//        ncpu = número de cores disponíveis
//        ucpu = a utilização da cpu (0.00 - 1.00) em decimal
//        W/C = wait time e compute time
        final Executor executor = Executors.newFixedThreadPool(Math.min(lojas.size(), 100), runnable -> {
            Thread thread = new Thread(runnable);
            thread.setDaemon(true);
            return thread;
        });

        acharPrecosCompletableFuture(lojas, executor);
    }

    private static List<String> acharPrecosStream(List<Loja> lojas) {
        System.out.println("Stream sequencial");
        long start = System.currentTimeMillis();

        List<String> collect = lojas.stream()
                .map(loja -> String.format("%s o preço é: %.2f", loja.getNome(), loja.getPreco()))
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo total: " + (end - start) + " ms");
        System.out.println(collect);
        return collect;
    }

    private static List<String> acharPrecosParallelStream(List<Loja> lojas) {
        System.out.println("Stream paralelo");
        long start = System.currentTimeMillis();

        List<String> collect = lojas.parallelStream()
                .map(loja -> String.format("%s o preço é: %.2f", loja.getNome(), loja.getPreco()))
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo total: " + (end - start) + " ms");
        System.out.println(collect);
        return collect;
    }

    private static List<String> acharPrecosCompletableFutureSeq(List<Loja> lojas) {
        System.out.println("Completable Future sequencial");
        long start = System.currentTimeMillis();

        List<String> collect = lojas.stream().map(loja -> CompletableFuture.supplyAsync(
                () -> String.format("%s o preço é: %.2f", loja.getNome(), loja.getPreco())))
                .map(CompletableFuture::join).collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo total: " + (end - start) + " ms");
        System.out.println(collect);
        return collect;
    }

    private static List<String> acharPrecosCompletableFuture(List<Loja> lojas) {
        System.out.println("Completable Future");
        long start = System.currentTimeMillis();

        List<CompletableFuture<String>> futureList = lojas.stream().map(loja -> CompletableFuture.supplyAsync(
                () -> String.format("%s o preço é: %.2f", loja.getNome(), loja.getPreco())))
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo de invocação: " + (end - start) + " ms");

        List<String> collect = futureList.stream().map(CompletableFuture::join).collect(Collectors.toList());

        System.out.println("Tempo total: " + (System.currentTimeMillis() - start) + " ms");
        System.out.println(collect);
        return collect;
    }

    private static List<String> acharPrecosCompletableFuture(List<Loja> lojas, Executor executor) {
        System.out.println("Completable Future com Executor");
        long start = System.currentTimeMillis();

        List<CompletableFuture<String>> futureList = lojas.stream().map(loja -> CompletableFuture.supplyAsync(
                () -> String.format("%s o preço é: %.2f", loja.getNome(), loja.getPreco()), executor))
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo de invocação: " + (end - start) + " ms");

        List<String> collect = futureList.stream().map(CompletableFuture::join).collect(Collectors.toList());

        System.out.println("Tempo total: " + (System.currentTimeMillis() - start) + " ms");
        System.out.println(collect);
        return collect;
    }
}
