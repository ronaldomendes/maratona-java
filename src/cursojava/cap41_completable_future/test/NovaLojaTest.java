package cursojava.cap41_completable_future.test;

import cursojava.cap41_completable_future.classes.Desconto;
import cursojava.cap41_completable_future.classes.NovaLoja;
import cursojava.cap41_completable_future.classes.Orcamento;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NovaLojaTest {
    public static void main(String[] args) {
        List<NovaLoja> lojas = NovaLoja.lojas();
//        lojas.stream().forEach(novaLoja -> System.out.println(novaLoja.getPreco()));
//        acharPrecos(lojas);

        final Executor executor = Executors.newFixedThreadPool(Math.min(lojas.size(), 100), runnable -> {
            Thread thread = new Thread(runnable);
            thread.setDaemon(true);
            return thread;
        });
//        acharPrecosAsync(lojas, executor);

        long start = System.currentTimeMillis();
        CompletableFuture.allOf(acharPrecosStream(lojas, executor)
                .map(future -> future.thenAccept(s -> System.out.println(
                        s + " (finalizado em: " + (System.currentTimeMillis() - start)+ " ms)")))
                .toArray(CompletableFuture[]::new)).join();
        long end = System.currentTimeMillis();
        System.out.println("Todas as lojas responderam em: " + (end - start) + " ms");
    }

    private static List<String> acharPrecos(List<NovaLoja> lojas) {
        System.out.println("Stream sequencial");
        long start = System.currentTimeMillis();

        List<String> collect = lojas.stream()
                .map(NovaLoja::getPreco)
                .map(Orcamento::parse)
                .map(Desconto::calcularDesconto)
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo total: " + (end - start) + " ms");
        System.out.println(collect);
        return collect;
    }

    private static List<String> acharPrecosAsync(List<NovaLoja> lojas, Executor executor) {
        System.out.println("Completable Future Async");
        long start = System.currentTimeMillis();

        List<CompletableFuture<String>> futures = lojas.stream()
//                Pegando o preço original de forma async
                .map(loja -> CompletableFuture.supplyAsync(loja::getPreco, executor))
//                Transforma a String em um orçamento no momento em que ele se torna disponível
                .map(future -> future.thenApply(Orcamento::parse))
//                Compõe o primeiro future com mais uma chamada async, para pegar os descontos
//                no momento que a primeira requisição async estiver disponível
                .map(future -> future.thenCompose(orcamento -> CompletableFuture.supplyAsync(
                        () -> Desconto.calcularDesconto(orcamento), executor)))
                .collect(Collectors.toList());

//        Espera todos os futures no stream finalizarem para terem seus resultados extraídos
        List<String> collect = futures.stream().map(CompletableFuture::join).collect(Collectors.toList());

        long end = System.currentTimeMillis();
        System.out.println("Tempo total: " + (end - start) + " ms");
        System.out.println(collect);
        return collect;
    }

    private static Stream<CompletableFuture<String>> acharPrecosStream(List<NovaLoja> lojas, Executor executor) {
        System.out.println("Completable Future Async Stream");
        long start = System.currentTimeMillis();

        Stream<CompletableFuture<String>> futureStream = lojas.stream()
//                Pegando o preço original de forma async
                .map(loja -> CompletableFuture.supplyAsync(loja::getPreco, executor))
//                Transforma a String em um orçamento no momento em que ele se torna disponível
                .map(future -> future.thenApply(Orcamento::parse))
//                Compõe o primeiro future com mais uma chamada async, para pegar os descontos
//                no momento que a primeira requisição async estiver disponível
                .map(future -> future.thenCompose(orcamento -> CompletableFuture.supplyAsync(
                        () -> Desconto.calcularDesconto(orcamento), executor)));


        long end = System.currentTimeMillis();
        System.out.println("Tempo total: " + (end - start) + " ms");
        return futureStream;
    }
}
