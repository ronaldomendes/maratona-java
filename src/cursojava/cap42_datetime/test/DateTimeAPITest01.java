package cursojava.cap42_datetime.test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoField;

public class DateTimeAPITest01 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(1999, Month.SEPTEMBER, 17);
        System.out.println("============ LocalDate ============");
        System.out.println(date.getYear());
        System.out.println(date.getMonth());
        System.out.println(date.getMonthValue());
        System.out.println(date.getDayOfMonth());
        System.out.println(date.getDayOfWeek());
        System.out.println(date.lengthOfMonth());
        System.out.println(date.lengthOfYear());
        System.out.println(date.isLeapYear());
        System.out.println(date.get(ChronoField.YEAR));
        System.out.println(date.get(ChronoField.DAY_OF_MONTH));
        System.out.println(date.get(ChronoField.YEAR_OF_ERA));

        LocalDate now = LocalDate.now();
        System.out.println(date);
        System.out.println(now);

        System.out.println(LocalDate.MAX);
        System.out.println(LocalDate.MIN);

        System.out.println("============ LocalTime ============");
        LocalTime time = LocalTime.of(23, 2, 12);
        System.out.println(time);
        System.out.println(time.getHour());
        System.out.println(time.getMinute());
        System.out.println(time.getSecond());
        System.out.println(LocalTime.MAX);
        System.out.println(LocalTime.MIN);

        System.out.println("============ Parsing ============");
        LocalDate parseDate = LocalDate.parse("2002-10-09");
        LocalTime parseTime = LocalTime.parse("23:58:01");
        System.out.println(parseDate);
        System.out.println(parseTime);

        System.out.println("============ LocalDateTime ============");
        LocalDateTime ldt01 = LocalDateTime.now();
        LocalDateTime ldt02 = LocalDateTime.of(2047, 6, 8, 7, 42, 28);
        LocalDateTime ldt03 = date.atTime(10, 20, 20);
        LocalDateTime ldt04 = date.atTime(LocalTime.now());
        LocalDateTime ldt05 = time.atDate(date);
        LocalDateTime ldt06 = date.atTime(time);

        System.out.println(ldt01);
        System.out.println(ldt02);
        System.out.println(ldt03);
        System.out.println(ldt04);
        System.out.println(ldt05);
        System.out.println(ldt06);
    }
}
