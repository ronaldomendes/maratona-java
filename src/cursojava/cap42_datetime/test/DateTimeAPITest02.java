package cursojava.cap42_datetime.test;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class DateTimeAPITest02 {
    public static void main(String[] args) {
        System.out.println("============== Instant ===============");
        Instant instant = Instant.now();
        System.out.println(instant);
        System.out.println(LocalDateTime.now());
        System.out.println(instant.getEpochSecond());
        System.out.println(instant.getNano());
        System.out.println(Instant.ofEpochSecond(3));
        System.out.println(Instant.ofEpochSecond(3, 0));
        System.out.println(Instant.ofEpochSecond(2, 1_000_000_000));
        System.out.println(Instant.ofEpochSecond(2, 90_000_000));
        System.out.println(Instant.ofEpochSecond(4, -1_000_000_000));

        System.out.println("============== Duration ===============");
        LocalDateTime ldt1 = LocalDateTime.now();
        LocalDateTime ldt2 = LocalDateTime.of(2019, 6, 1, 23, 58, 12);
        LocalTime time1 = LocalTime.now();
        LocalTime time2 = LocalTime.of(12, 38, 25);

//        Não pode usar LocalDate
//        Não pode misturar LocalDateTime com LocalTime
        Duration d1 = Duration.between(ldt1, ldt2);
        Duration d2 = Duration.between(time1, time2);
        Duration d3 = Duration.between(Instant.now(), Instant.now().plusSeconds(1000));
//        Duration d4 = Duration.between(ldt2, time2);
//        Duration d5 = Duration.between(LocalDate.now(), LocalDate.now().plusDays(20));
        Duration d6 = Duration.between(ldt2, time2.atDate(ldt2.toLocalDate()));
        Duration d7 = Duration.ofMinutes(4);
        Duration d8 = Duration.ofDays(14);

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
        System.out.println(d6);
        System.out.println(d7);
        System.out.println(d8);

        System.out.println("============== Duration ===============");
        Period p1 = Period.between(ldt1.toLocalDate(), ldt2.toLocalDate());
        Period p2 = Period.ofDays(10);
        Period p3 = Period.ofWeeks(58);
        Period p4 = Period.ofYears(3);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
        System.out.println(Period.between(LocalDate.now(), LocalDate.now().plusDays(p3.getDays())).getMonths());
        System.out.println(Period.between(LocalDate.now(), LocalDate.now().plusDays(p3.getDays())));

        LocalDateTime now = LocalDateTime.now();
        System.out.println(now.until(now.plusDays(p3.getDays()), ChronoUnit.MONTHS));

        System.out.println("============== ChronoUnit ===============");
        LocalDateTime aniversario = LocalDateTime.of(1987, 6, 19, 21, 45, 0);
        System.out.println(ChronoUnit.DAYS.between(aniversario, now));
        System.out.println(ChronoUnit.MONTHS.between(aniversario, now));
        System.out.println(ChronoUnit.WEEKS.between(aniversario, now));
        System.out.println(ChronoUnit.YEARS.between(aniversario, now));
    }
}
