package cursojava.cap42_datetime.test;

import java.time.*;
import java.time.chrono.JapaneseDate;
import java.util.Map;

public class DateTimeAPITest06 {
    public static void main(String[] args) {
        for (Map.Entry<String, String> zonas : ZoneId.SHORT_IDS.entrySet()) {
            System.out.println(zonas.getKey() + ": " + zonas.getValue());
        }

        System.out.println("===============================");
        System.out.println(ZoneId.systemDefault());
        ZoneId tokyoZone = ZoneId.of("Asia/Tokyo");
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println(dateTime);
        ZonedDateTime zonedDateTime = dateTime.atZone(tokyoZone);
        System.out.println(zonedDateTime);

        Instant instant = Instant.now();
        System.out.println(instant);
        ZonedDateTime zonedDateTime1 = instant.atZone(tokyoZone);
        System.out.println(zonedDateTime1);

        System.out.println(ZoneOffset.MIN);
        System.out.println(ZoneOffset.MAX);

        ZoneOffset manausOffset = ZoneOffset.of("-04:00");
        LocalDateTime dateTime1 = LocalDateTime.now();
        OffsetDateTime offsetDateTime = OffsetDateTime.of(dateTime1, manausOffset);
        OffsetDateTime offsetDateTime1 = dateTime1.atOffset(manausOffset);
        System.out.println(offsetDateTime);
        System.out.println(offsetDateTime1);

        Instant instant1 = Instant.now();
        System.out.println(instant1.atOffset(manausOffset));

        JapaneseDate japaneseDate = JapaneseDate.from(LocalDate.now());
        System.out.println(japaneseDate);
        LocalDate antigamente = LocalDate.of(1900, 2, 1);
        System.out.println(JapaneseDate.from(antigamente));
    }
}
